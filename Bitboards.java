package game2;

public class Bitboards {

	protected long WHITE_PAWNS;protected long WHITE_ROOKS;;protected long WHITE_KNIGHTS;protected long WHITE_BISHOPS;protected long WHITE_QUEENS;protected long WHITE_KING;
	protected long BLACK_PAWNS;protected long BLACK_ROOKS;protected long BLACK_KNIGHTS;protected long BLACK_BISHOPS;protected long BLACK_QUEENS;protected long BLACK_KING;
	
	protected long FULLBOARD;
	protected long EMPTY_SQUARES;
	protected long ALL_PIECES;
	protected long WHITE_PIECES;
	protected long BLACK_PIECES;
	
	Bitboards(){
		System.out.println("In Bitboards()");
		
		WHITE_PAWNS 	= Long.decode("0x000000000000FF00");
		WHITE_ROOKS 	= Long.decode("0x0000000000000081");
		WHITE_KNIGHTS 	= Long.decode("0x0000000000000042");
		WHITE_BISHOPS 	= Long.decode("0x0000000000000024");
		WHITE_QUEENS 	= Long.decode("0x0000000000000008");
		WHITE_KING 		= Long.decode("0x0000000000000010");
		
		BLACK_PAWNS 	= Long.decode("0x00FF000000000000");
		BLACK_ROOKS 	= Long.decode("-0x7F00000000000000");
		BLACK_KNIGHTS 	= Long.decode("0x4200000000000000");
		BLACK_BISHOPS 	= Long.decode("0x2400000000000000");
		BLACK_QUEENS 	= Long.decode("0x0800000000000000");
		BLACK_KING 		= Long.decode("0x1000000000000000");
		
		FULLBOARD		= Long.decode("-0x0000000000000001");
		WHITE_PIECES();
		BLACK_PIECES();
		ALL_PIECES();
		EMPTY_SQUARES();
		
		System.out.println("out of Bitboards()");
	}
	
	Bitboards(Pieceboard startingBoard){
		WHITE_PAWNS 	= 0;
		WHITE_ROOKS 	= 0;
		WHITE_KNIGHTS 	= 0;
		WHITE_BISHOPS 	= 0;
		WHITE_QUEENS 	= 0;
		WHITE_KING 		= 0;
		
		BLACK_PAWNS 	= 0;
		BLACK_ROOKS 	= 0;
		BLACK_KNIGHTS 	= 0;
		BLACK_BISHOPS 	= 0;
		BLACK_QUEENS 	= 0;
		BLACK_KING 		= 0;
		
		FULLBOARD		= Long.decode("-0x0000000000000001");
		FigureOutPieceBoard(startingBoard);
		UpdateComboBoards();
		printBoard();
	}
	
	Bitboards(Bitboards bb) {
		WHITE_PAWNS = bb.WHITE_PAWNS;
		WHITE_KNIGHTS = bb.WHITE_KNIGHTS;
		WHITE_ROOKS = bb.WHITE_ROOKS;
		WHITE_QUEENS = bb.WHITE_QUEENS;
		WHITE_KING = bb.WHITE_KING;
		WHITE_BISHOPS = bb.WHITE_BISHOPS;
		
		BLACK_PAWNS = bb.BLACK_PAWNS;
		BLACK_KNIGHTS = bb.BLACK_KNIGHTS;
		BLACK_ROOKS = bb.BLACK_ROOKS;
		BLACK_QUEENS = bb.BLACK_QUEENS;
		BLACK_KING = bb.BLACK_KING;
		BLACK_BISHOPS = bb.BLACK_BISHOPS;
		
		UpdateComboBoards();
	}

	private void WHITE_PIECES(){
		WHITE_PIECES = WHITE_PAWNS | WHITE_KNIGHTS | WHITE_BISHOPS | WHITE_ROOKS | WHITE_QUEENS | WHITE_KING;
	}
	private void BLACK_PIECES(){
		BLACK_PIECES = BLACK_PAWNS | BLACK_KNIGHTS | BLACK_BISHOPS | BLACK_ROOKS | BLACK_QUEENS | BLACK_KING;
	}	
	private void ALL_PIECES(){
		ALL_PIECES = WHITE_PIECES | BLACK_PIECES;
	}
	private void EMPTY_SQUARES(){
		EMPTY_SQUARES = ALL_PIECES ^ FULLBOARD;
	}
	
	public void UpdateComboBoards(){
		WHITE_PIECES();
		BLACK_PIECES();
		ALL_PIECES();
		EMPTY_SQUARES();
	}
	public void PrintAllBoardsForwards(){
		System.out.println("White Pieces");
		TestGame.printBitBoardForward(WHITE_PAWNS);
		TestGame.printBitBoardForward(WHITE_KNIGHTS);
		TestGame.printBitBoardForward(WHITE_BISHOPS);
		TestGame.printBitBoardForward(WHITE_ROOKS);
		TestGame.printBitBoardForward(WHITE_QUEENS);
		TestGame.printBitBoardForward(WHITE_KING);
		System.out.println("Black Pieces");
		TestGame.printBitBoardForward(BLACK_PAWNS);
		TestGame.printBitBoardForward(BLACK_KNIGHTS);
		TestGame.printBitBoardForward(BLACK_BISHOPS);
		TestGame.printBitBoardForward(BLACK_ROOKS);
		TestGame.printBitBoardForward(BLACK_QUEENS);
		TestGame.printBitBoardForward(BLACK_KING);
	}

	public void printBoard(){
		Pieceboard myPieceboard = new Pieceboard(this);
		myPieceboard.displayBoard();
	}
	private void FigureOutPieceBoard(Pieceboard pieces){
		
		for (int r = 0; r < 8; r++){
			for (int c = 0; c < 8; c++){
				
				int bitNumber = (7-r) * 8 + c;
				
				switch(pieces.Board[r][c]){
				case Pieceboard.EMPTY: ; break;
				
				case Pieceboard.WHITEPAWN: 		WHITE_PAWNS 	= WHITE_PAWNS 	| (long)1 << bitNumber; break;
				case Pieceboard.WHITEROOK: 		WHITE_ROOKS 	= WHITE_ROOKS 	| (long)1 << bitNumber; break;
				case Pieceboard.WHITEKNIGHT: 	WHITE_KNIGHTS 	= WHITE_KNIGHTS | (long)1 << bitNumber; break;
				case Pieceboard.WHITEBISHOP: 	WHITE_BISHOPS 	= WHITE_BISHOPS	| (long)1 << bitNumber; break;
				case Pieceboard.WHITEQUEEN: 	WHITE_QUEENS 	= WHITE_QUEENS 	| (long)1 << bitNumber; break;
				case Pieceboard.WHITEKING: 		WHITE_KING 		= WHITE_KING 	| (long)1 << bitNumber; break;
				
				case Pieceboard.BLACKPAWN: 		BLACK_PAWNS	 	= BLACK_PAWNS 	| (long)1 << bitNumber; break;
				case Pieceboard.BLACKROOK: 		BLACK_ROOKS 	= BLACK_ROOKS 	| (long)1 << bitNumber; break;
				case Pieceboard.BLACKKNIGHT: 	BLACK_KNIGHTS 	= BLACK_KNIGHTS | (long)1 << bitNumber; break;
				case Pieceboard.BLACKBISHOP: 	BLACK_BISHOPS	= BLACK_BISHOPS | (long)1 << bitNumber; break;
				case Pieceboard.BLACKQUEEN: 	BLACK_QUEENS	= BLACK_QUEENS  | (long)1 << bitNumber; break;
				case Pieceboard.BLACKKING: 		BLACK_KING 		= BLACK_KING	| (long)1 << bitNumber; break;
				}
			}
		}	
	}
	public long checkTestWhiteSquare2(int CurrentBit){
    	
    	//PAWN ATTACKING KING
		long Attacks = MoveMaps.getWhPawn_Attacks()[CurrentBit] & BLACK_PAWNS;
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & BLACK_KNIGHTS;
		//KING ATTACKING KING
		Attacks = Attacks | MoveMaps.getKing_Attacks()[CurrentBit] & BLACK_KING;
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & BLACK_KNIGHTS;
		//BISHOPS OR QUEENS ATTACKING KING
		long UpRight;
		
		UpRight = MoveMaps.getUpRight_Moves()[CurrentBit] & ALL_PIECES;
		UpRight = (UpRight << 9) | (UpRight << 18) | (UpRight << 27) | (UpRight << 36) | (UpRight << 45) | (UpRight << 54);
		UpRight = UpRight & MoveMaps.getUpRight_Moves()[CurrentBit];
		UpRight = UpRight ^ MoveMaps.getUpRight_Moves()[CurrentBit];
		
		UpRight = UpRight & (BLACK_BISHOPS | BLACK_QUEENS);
		
		long DownLeft;
		
		DownLeft = MoveMaps.getDownLeft_Moves()[CurrentBit] & ALL_PIECES;
		DownLeft = (DownLeft >>> 9) | (DownLeft >>> 18) | (DownLeft >>> 27) | (DownLeft >>> 36) | (DownLeft >>> 45) | (DownLeft >>> 54);
		DownLeft = DownLeft & MoveMaps.getDownLeft_Moves()[CurrentBit];
		DownLeft = DownLeft ^ MoveMaps.getDownLeft_Moves()[CurrentBit];

		DownLeft = DownLeft & BLACK_BISHOPS | BLACK_QUEENS;
		
		long UpLeft;
		
		UpLeft = MoveMaps.getUpLeft_Moves()[CurrentBit] & ALL_PIECES;
		UpLeft = (UpLeft << 7) | (UpLeft << 14) | (UpLeft << 21) | (UpLeft << 28) | (UpLeft << 35) | (UpLeft << 42);
		UpLeft = UpLeft & MoveMaps.getUpLeft_Moves()[CurrentBit];
		UpLeft = UpLeft ^ MoveMaps.getUpLeft_Moves()[CurrentBit];

		UpLeft = UpLeft & (BLACK_BISHOPS | BLACK_QUEENS);
		
		long DownRight;
		
		DownRight = MoveMaps.getDownRight_Moves()[CurrentBit] & ALL_PIECES;
		DownRight = (DownRight >>> 7) | (DownRight >>> 14) | (DownRight >>> 21) | (DownRight >>> 28) | (DownRight >>> 35) | (DownRight >>> 42);
		DownRight = DownRight & MoveMaps.getDownRight_Moves()[CurrentBit];
		DownRight = DownRight ^ MoveMaps.getDownRight_Moves()[CurrentBit];

		DownRight = DownRight & (BLACK_BISHOPS | BLACK_QUEENS);
		
		Attacks = Attacks | UpRight | DownLeft | UpLeft | DownRight;
		
		//ROOKS AND QUEENS ATTACKING KING
		long right;
		
		right = MoveMaps.getRight_Moves()[CurrentBit] & ALL_PIECES;
		right = (right << 1) | (right << 2) | (right << 3) | (right << 4) | (right << 5) | (right << 6);
		right = right & MoveMaps.getRight_Moves()[CurrentBit];
		right = right ^ MoveMaps.getRight_Moves()[CurrentBit];
		
		right = right & (BLACK_ROOKS | BLACK_QUEENS);
		
		long left;
		
		left = MoveMaps.getLeft_Moves()[CurrentBit] & ALL_PIECES;
		left = (left >>> 1) | (left >>> 2) | (left >>> 3) | (left >>> 4) | (left >>> 5) | (left >>> 6);
		left = left & MoveMaps.getLeft_Moves()[CurrentBit];
		left = left ^ MoveMaps.getLeft_Moves()[CurrentBit];

		left = left & (BLACK_ROOKS | BLACK_QUEENS);
		
		long up;
		
		up = MoveMaps.getUp_Moves()[CurrentBit] & ALL_PIECES;
		up = (up << 8) | (up << 16) | (up << 24) | (up << 32) | (up << 40) | (up << 48);
		up = up & MoveMaps.getUp_Moves()[CurrentBit];
		up = up ^ MoveMaps.getUp_Moves()[CurrentBit];

		up = up & (BLACK_ROOKS | BLACK_QUEENS);
		
		long down;
		
		down = MoveMaps.getDown_Moves()[CurrentBit] & ALL_PIECES;
		down = (down >>> 8) | (down >>> 16) | (down >>> 24) | (down >>> 32) | (down >>> 40) | (down >>> 48);
		down = down & MoveMaps.getDown_Moves()[CurrentBit];
		down = down ^ MoveMaps.getDown_Moves()[CurrentBit];

		down = down & (BLACK_ROOKS | BLACK_QUEENS);
		
		Attacks = Attacks | up | down | right | left;
		
		return Attacks;

    }
	public long checkTestWhite2(){
    	
    	int CurrentBit = Long.numberOfTrailingZeros(WHITE_KING);
    	//PAWN ATTACKING KING
		long Attacks = MoveMaps.getWhPawn_Attacks()[CurrentBit] & BLACK_PAWNS;
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & BLACK_KNIGHTS;
		//KING ATTACKING KING
		Attacks = Attacks | MoveMaps.getKing_Attacks()[CurrentBit] & BLACK_KING;
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & BLACK_KNIGHTS;
		//BISHOPS OR QUEENS ATTACKING KING
		long UpRight;
		
		UpRight = MoveMaps.getUpRight_Moves()[CurrentBit] & ALL_PIECES;
		UpRight = (UpRight << 9) | (UpRight << 18) | (UpRight << 27) | (UpRight << 36) | (UpRight << 45) | (UpRight << 54);
		UpRight = UpRight & MoveMaps.getUpRight_Moves()[CurrentBit];
		UpRight = UpRight ^ MoveMaps.getUpRight_Moves()[CurrentBit];
		
		UpRight = UpRight & (BLACK_BISHOPS | BLACK_QUEENS);
		
		long DownLeft;
		
		DownLeft = MoveMaps.getDownLeft_Moves()[CurrentBit] & ALL_PIECES;
		DownLeft = (DownLeft >>> 9) | (DownLeft >>> 18) | (DownLeft >>> 27) | (DownLeft >>> 36) | (DownLeft >>> 45) | (DownLeft >>> 54);
		DownLeft = DownLeft & MoveMaps.getDownLeft_Moves()[CurrentBit];
		DownLeft = DownLeft ^ MoveMaps.getDownLeft_Moves()[CurrentBit];

		DownLeft = DownLeft & BLACK_BISHOPS | BLACK_QUEENS;
		
		long UpLeft;
		
		UpLeft = MoveMaps.getUpLeft_Moves()[CurrentBit] & ALL_PIECES;
		UpLeft = (UpLeft << 7) | (UpLeft << 14) | (UpLeft << 21) | (UpLeft << 28) | (UpLeft << 35) | (UpLeft << 42);
		UpLeft = UpLeft & MoveMaps.getUpLeft_Moves()[CurrentBit];
		UpLeft = UpLeft ^ MoveMaps.getUpLeft_Moves()[CurrentBit];

		UpLeft = UpLeft & (BLACK_BISHOPS | BLACK_QUEENS);
		
		long DownRight;
		
		DownRight = MoveMaps.getDownRight_Moves()[CurrentBit] & ALL_PIECES;
		DownRight = (DownRight >>> 7) | (DownRight >>> 14) | (DownRight >>> 21) | (DownRight >>> 28) | (DownRight >>> 35) | (DownRight >>> 42);
		DownRight = DownRight & MoveMaps.getDownRight_Moves()[CurrentBit];
		DownRight = DownRight ^ MoveMaps.getDownRight_Moves()[CurrentBit];

		DownRight = DownRight & (BLACK_BISHOPS | BLACK_QUEENS);
		
		Attacks = Attacks | UpRight | DownLeft | UpLeft | DownRight;
		
		//ROOKS AND QUEENS ATTACKING KING
		long right;
		
		right = MoveMaps.getRight_Moves()[CurrentBit] & ALL_PIECES;
		right = (right << 1) | (right << 2) | (right << 3) | (right << 4) | (right << 5) | (right << 6);
		right = right & MoveMaps.getRight_Moves()[CurrentBit];
		right = right ^ MoveMaps.getRight_Moves()[CurrentBit];
		
		right = right & (BLACK_ROOKS | BLACK_QUEENS);
		
		long left;
		
		left = MoveMaps.getLeft_Moves()[CurrentBit] & ALL_PIECES;
		left = (left >>> 1) | (left >>> 2) | (left >>> 3) | (left >>> 4) | (left >>> 5) | (left >>> 6);
		left = left & MoveMaps.getLeft_Moves()[CurrentBit];
		left = left ^ MoveMaps.getLeft_Moves()[CurrentBit];

		left = left & (BLACK_ROOKS | BLACK_QUEENS);
		
		long up;
		
		up = MoveMaps.getUp_Moves()[CurrentBit] & ALL_PIECES;
		up = (up << 8) | (up << 16) | (up << 24) | (up << 32) | (up << 40) | (up << 48);
		up = up & MoveMaps.getUp_Moves()[CurrentBit];
		up = up ^ MoveMaps.getUp_Moves()[CurrentBit];

		up = up & (BLACK_ROOKS | BLACK_QUEENS);
		
		long down;
		
		down = MoveMaps.getDown_Moves()[CurrentBit] & ALL_PIECES;
		down = (down >>> 8) | (down >>> 16) | (down >>> 24) | (down >>> 32) | (down >>> 40) | (down >>> 48);
		down = down & MoveMaps.getDown_Moves()[CurrentBit];
		down = down ^ MoveMaps.getDown_Moves()[CurrentBit];

		down = down & (BLACK_ROOKS | BLACK_QUEENS);
		
		Attacks = Attacks | up | down | right | left;
		
		return Attacks;

    }
	public long checkTestBlackSquare2(int CurrentBit){
		
    	//PAWN ATTACKING KING
		long Attacks = MoveMaps.getBlPawn_Attacks()[CurrentBit] & WHITE_PAWNS;
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & WHITE_KNIGHTS;
		//KING ATTACKING KING
		Attacks = Attacks | MoveMaps.getKing_Attacks()[CurrentBit] & WHITE_KING;
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & WHITE_KNIGHTS;
		//BISHOPS OR QUEENS ATTACKING KING
		long UpRight;
		
		UpRight = MoveMaps.getUpRight_Moves()[CurrentBit] & ALL_PIECES;
		UpRight = (UpRight << 9) | (UpRight << 18) | (UpRight << 27) | (UpRight << 36) | (UpRight << 45) | (UpRight << 54);
		UpRight = UpRight & MoveMaps.getUpRight_Moves()[CurrentBit];
		UpRight = UpRight ^ MoveMaps.getUpRight_Moves()[CurrentBit];
		
		UpRight = UpRight & (WHITE_BISHOPS | WHITE_QUEENS);
		
		long DownLeft;
		
		DownLeft = MoveMaps.getDownLeft_Moves()[CurrentBit] & ALL_PIECES;
		DownLeft = (DownLeft >>> 9) | (DownLeft >>> 18) | (DownLeft >>> 27) | (DownLeft >>> 36) | (DownLeft >>> 45) | (DownLeft >>> 54);
		DownLeft = DownLeft & MoveMaps.getDownLeft_Moves()[CurrentBit];
		DownLeft = DownLeft ^ MoveMaps.getDownLeft_Moves()[CurrentBit];

		DownLeft = DownLeft & WHITE_BISHOPS | WHITE_QUEENS;
		
		long UpLeft;
		
		UpLeft = MoveMaps.getUpLeft_Moves()[CurrentBit] & ALL_PIECES;
		UpLeft = (UpLeft << 7) | (UpLeft << 14) | (UpLeft << 21) | (UpLeft << 28) | (UpLeft << 35) | (UpLeft << 42);
		UpLeft = UpLeft & MoveMaps.getUpLeft_Moves()[CurrentBit];
		UpLeft = UpLeft ^ MoveMaps.getUpLeft_Moves()[CurrentBit];

		UpLeft = UpLeft & (WHITE_BISHOPS | WHITE_QUEENS);
		
		long DownRight;
		
		DownRight = MoveMaps.getDownRight_Moves()[CurrentBit] & ALL_PIECES;
		DownRight = (DownRight >>> 7) | (DownRight >>> 14) | (DownRight >>> 21) | (DownRight >>> 28) | (DownRight >>> 35) | (DownRight >>> 42);
		DownRight = DownRight & MoveMaps.getDownRight_Moves()[CurrentBit];
		DownRight = DownRight ^ MoveMaps.getDownRight_Moves()[CurrentBit];

		DownRight = DownRight & (WHITE_BISHOPS | WHITE_QUEENS);
		
		Attacks = Attacks | UpRight | DownLeft | UpLeft | DownRight;
		
		//ROOKS AND QUEENS ATTACKING KING
		long right;
		
		right = MoveMaps.getRight_Moves()[CurrentBit] & ALL_PIECES;
		right = (right << 1) | (right << 2) | (right << 3) | (right << 4) | (right << 5) | (right << 6);
		right = right & MoveMaps.getRight_Moves()[CurrentBit];
		right = right ^ MoveMaps.getRight_Moves()[CurrentBit];
		
		right = right & (WHITE_ROOKS | WHITE_QUEENS);
		
		long left;
		
		left = MoveMaps.getLeft_Moves()[CurrentBit] & ALL_PIECES;
		left = (left >>> 1) | (left >>> 2) | (left >>> 3) | (left >>> 4) | (left >>> 5) | (left >>> 6);
		left = left & MoveMaps.getLeft_Moves()[CurrentBit];
		left = left ^ MoveMaps.getLeft_Moves()[CurrentBit];

		left = left & (WHITE_ROOKS | WHITE_QUEENS);
		
		long up;
		
		up = MoveMaps.getUp_Moves()[CurrentBit] & ALL_PIECES;
		up = (up << 8) | (up << 16) | (up << 24) | (up << 32) | (up << 40) | (up << 48);
		up = up & MoveMaps.getUp_Moves()[CurrentBit];
		up = up ^ MoveMaps.getUp_Moves()[CurrentBit];

		up = up & (WHITE_ROOKS | WHITE_QUEENS);
		
		long down;
		
		down = MoveMaps.getDown_Moves()[CurrentBit] & ALL_PIECES;
		down = (down >>> 8) | (down >>> 16) | (down >>> 24) | (down >>> 32) | (down >>> 40) | (down >>> 48);
		down = down & MoveMaps.getDown_Moves()[CurrentBit];
		down = down ^ MoveMaps.getDown_Moves()[CurrentBit];

		down = down & (WHITE_ROOKS | WHITE_QUEENS);
		
		Attacks = Attacks | up | down | right | left;
		
		return Attacks;
    }
    public long checkTestBlack2(){
    	int CurrentBit = Long.numberOfTrailingZeros(BLACK_KING);
    	TestGame.printBitBoardForward(BLACK_KING);
    	System.out.println("CurrentBit ="  + CurrentBit);
    	//PAWN ATTACKING KING
		long Attacks = MoveMaps.getBlPawn_Attacks()[CurrentBit] & WHITE_PAWNS;
		
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & WHITE_KNIGHTS;
		
		//KING ATTACKING KING
		Attacks = Attacks | MoveMaps.getKing_Attacks()[CurrentBit] & WHITE_KING;
		
		//KNIGHTS ATTACKING KING
		Attacks = Attacks | MoveMaps.getKnight_Attacks()[CurrentBit] & WHITE_KNIGHTS;
		
		//BISHOPS OR QUEENS ATTACKING KING
		long UpRight;
		
		UpRight = MoveMaps.getUpRight_Moves()[CurrentBit] & ALL_PIECES;
		UpRight = (UpRight << 9) | (UpRight << 18) | (UpRight << 27) | (UpRight << 36) | (UpRight << 45) | (UpRight << 54);
		UpRight = UpRight & MoveMaps.getUpRight_Moves()[CurrentBit];
		UpRight = UpRight ^ MoveMaps.getUpRight_Moves()[CurrentBit];
		
		UpRight = UpRight & (WHITE_BISHOPS | WHITE_QUEENS);
		
		long DownLeft;
		
		DownLeft = MoveMaps.getDownLeft_Moves()[CurrentBit] & ALL_PIECES;
		DownLeft = (DownLeft >>> 9) | (DownLeft >>> 18) | (DownLeft >>> 27) | (DownLeft >>> 36) | (DownLeft >>> 45) | (DownLeft >>> 54);
		DownLeft = DownLeft & MoveMaps.getDownLeft_Moves()[CurrentBit];
		DownLeft = DownLeft ^ MoveMaps.getDownLeft_Moves()[CurrentBit];

		DownLeft = DownLeft & WHITE_BISHOPS | WHITE_QUEENS;
		
		long UpLeft;
		
		UpLeft = MoveMaps.getUpLeft_Moves()[CurrentBit] & ALL_PIECES;
		UpLeft = (UpLeft << 7) | (UpLeft << 14) | (UpLeft << 21) | (UpLeft << 28) | (UpLeft << 35) | (UpLeft << 42);
		UpLeft = UpLeft & MoveMaps.getUpLeft_Moves()[CurrentBit];
		UpLeft = UpLeft ^ MoveMaps.getUpLeft_Moves()[CurrentBit];

		UpLeft = UpLeft & (WHITE_BISHOPS | WHITE_QUEENS);
		
		long DownRight;
		
		DownRight = MoveMaps.getDownRight_Moves()[CurrentBit] & ALL_PIECES;
		DownRight = (DownRight >>> 7) | (DownRight >>> 14) | (DownRight >>> 21) | (DownRight >>> 28) | (DownRight >>> 35) | (DownRight >>> 42);
		DownRight = DownRight & MoveMaps.getDownRight_Moves()[CurrentBit];
		DownRight = DownRight ^ MoveMaps.getDownRight_Moves()[CurrentBit];

		DownRight = DownRight & (WHITE_BISHOPS | WHITE_QUEENS);
		
		Attacks = Attacks | UpRight | DownLeft | UpLeft | DownRight;
		
		//ROOKS AND QUEENS ATTACKING KING
		long right;
		
		right = MoveMaps.getRight_Moves()[CurrentBit] & ALL_PIECES;
		right = (right << 1) | (right << 2) | (right << 3) | (right << 4) | (right << 5) | (right << 6);
		right = right & MoveMaps.getRight_Moves()[CurrentBit];
		right = right ^ MoveMaps.getRight_Moves()[CurrentBit];
		
		right = right & (WHITE_ROOKS | WHITE_QUEENS);
		
		long left;
		
		left = MoveMaps.getLeft_Moves()[CurrentBit] & ALL_PIECES;
		left = (left >>> 1) | (left >>> 2) | (left >>> 3) | (left >>> 4) | (left >>> 5) | (left >>> 6);
		left = left & MoveMaps.getLeft_Moves()[CurrentBit];
		left = left ^ MoveMaps.getLeft_Moves()[CurrentBit];

		left = left & (WHITE_ROOKS | WHITE_QUEENS);
		
		long up;
		
		up = MoveMaps.getUp_Moves()[CurrentBit] & ALL_PIECES;
		up = (up << 8) | (up << 16) | (up << 24) | (up << 32) | (up << 40) | (up << 48);
		up = up & MoveMaps.getUp_Moves()[CurrentBit];
		up = up ^ MoveMaps.getUp_Moves()[CurrentBit];

		up = up & (WHITE_ROOKS | WHITE_QUEENS);
		
		long down;
		
		down = MoveMaps.getDown_Moves()[CurrentBit] & ALL_PIECES;
		down = (down >>> 8) | (down >>> 16) | (down >>> 24) | (down >>> 32) | (down >>> 40) | (down >>> 48);
		down = down & MoveMaps.getDown_Moves()[CurrentBit];
		down = down ^ MoveMaps.getDown_Moves()[CurrentBit];

		down = down & (WHITE_ROOKS | WHITE_QUEENS);
		
		Attacks = Attacks | up | down | right | left;
		
		return Attacks;
    }

    public Bitboards makeMove(Move myMove) {
    	
    	if(myMove.getFrom() == myMove.getTo()){
    		return makeCastleMove(myMove);
    	}
    	else if(myMove.getTakes() == 1 && myMove.getRank() == Move.pawn && (((long)1 << myMove.getTo()) & ALL_PIECES) == 0){
    		return makeEnPassentMove(myMove);
    	}
    	else return makeNormalMove(myMove);
    }
    public Bitboards undoMove(Move myMove) {
    	
    	if(myMove.getFrom() == myMove.getTo()){
    		return undoCastleMove(myMove);
    	}
    	else if(myMove.getTakes() == 1 && myMove.getRank() == Move.pawn && (myMove.getTo() == (myMove.getGameState() >> GameInfo.EnPassentAt & 63))){
    		return undoEnPassentMove(myMove);
    	}
    	else return undoNormalMove(myMove);
    }
    
    public Bitboards makeEnPassentMove(Move myMove){
    	int whTurn = (myMove.getGameState() >>> 13);
    	if(whTurn == 1)
    		return makeWhiteEnPassentMove(myMove);
    	else
    		return makeBlackEnPassentMove(myMove);
    }
    public Bitboards undoEnPassentMove(Move myMove){
    	int whTurn = (myMove.getGameState() >>> 13);
    	if(whTurn == 1)
    		return undoWhiteEnPassentMove(myMove);
    	else
    		return undoBlackEnPassentMove(myMove);
    }
    
    public Bitboards makeWhiteEnPassentMove(Move myMove){
    	long fromBoard 	= (long)1 << myMove.getFrom();
    	long toBoard	= (long)1 << myMove.getTo();
    	WHITE_PAWNS = (WHITE_PAWNS ^ fromBoard) ^ toBoard;
    	BLACK_PAWNS = BLACK_PAWNS ^ (toBoard >> 8);
    	UpdateComboBoards();
    	return this;
    }
    public Bitboards makeBlackEnPassentMove(Move myMove){
    	long fromBoard 	= (long)1 << myMove.getFrom();
    	long toBoard	= (long)1 << myMove.getTo();
    	BLACK_PAWNS = (BLACK_PAWNS ^ fromBoard) ^ toBoard;
    	WHITE_PAWNS = WHITE_PAWNS ^ (toBoard << 8);
    	UpdateComboBoards();
    	return this;
    }
    
    
    public Bitboards undoWhiteEnPassentMove(Move myMove){
    	long fromBoard 	= (long)1 << myMove.getTo();
    	long toBoard	= (long)1 << myMove.getFrom();
    	WHITE_PAWNS = (WHITE_PAWNS ^ fromBoard) ^ toBoard;
    	BLACK_PAWNS = BLACK_PAWNS ^ (fromBoard >> 8);
    	UpdateComboBoards();
    	return this;
    }
    public Bitboards undoBlackEnPassentMove(Move myMove){
    	long fromBoard 	= (long)1 << myMove.getTo();
    	long toBoard	= (long)1 << myMove.getFrom();
    	BLACK_PAWNS = (BLACK_PAWNS ^ fromBoard) ^ toBoard;
    	WHITE_PAWNS = WHITE_PAWNS ^ (fromBoard << 8);
    	UpdateComboBoards();
    	return this;
    }

    
    public Bitboards makeCastleMove(Move myMove){
    	int whTurn = (myMove.getGameState() >>> 13) ^ 1;
    	if(whTurn == 1){
    		if(myMove.getRank() == Move.qsCastle)
    			return makeWhiteQS();
    		else
    			return makeWhiteKS();
    	}
    	else{
    		if(myMove.getRank() == Move.qsCastle)
    			return makeBlackQS();
    		else
    			return makeBlackKS();
    	}
    }
    public Bitboards undoCastleMove(Move myMove){
    	int whTurn = (myMove.getGameState() >>> 13) ^ 1;
    	if(whTurn == 1){
    		if(myMove.getRank() == Move.qsCastle)
    			return undoWhiteQS();
    		else
    			return undoWhiteKS();
    	}
    	else{
    		if(myMove.getRank() == Move.qsCastle)
    			return undoBlackQS();
    		else
    			return undoBlackKS();
    	}
    }
    
    public Bitboards makeWhiteQS(){
    	WHITE_KING = WHITE_KING >> 2;
    	WHITE_ROOKS = MoveMaps.WhiteQueenRook ^ WHITE_ROOKS ^ ((long)1 << 3);
    	UpdateComboBoards();
    	return this;
    }    
    public Bitboards makeWhiteKS(){
    	WHITE_KING = WHITE_KING << 2;
    	WHITE_ROOKS = MoveMaps.WhiteKingRook ^ WHITE_ROOKS ^ ((long)1 << 5);
    	UpdateComboBoards();
    	return this;
    }
    public Bitboards makeBlackQS(){
    	BLACK_KING = BLACK_KING >> 2;
    	BLACK_ROOKS = MoveMaps.BlackQueenRook ^ BLACK_ROOKS ^ ((long)1 << 59);
    	UpdateComboBoards();
    	return this;
    }
    public Bitboards makeBlackKS(){
    	BLACK_KING = BLACK_KING << 2;
    	BLACK_ROOKS = MoveMaps.BlackKingRook ^ BLACK_ROOKS ^ ((long)1 << 61);
    	UpdateComboBoards();
    	return this;
    }
    public Bitboards undoWhiteQS(){
    	WHITE_KING = WHITE_KING << 2;
    	WHITE_ROOKS = MoveMaps.WhiteQueenRook ^ WHITE_ROOKS ^ ((long)1 << 3);
    	UpdateComboBoards();
    	return this;
    }    
    public Bitboards undoWhiteKS(){
    	WHITE_KING = WHITE_KING >> 2;
    	WHITE_ROOKS = MoveMaps.WhiteKingRook ^ WHITE_ROOKS ^ ((long)1 << 5);
    	UpdateComboBoards();
    	return this;
    }
    public Bitboards undoBlackQS(){
    	BLACK_KING = BLACK_KING << 2;
    	BLACK_ROOKS = MoveMaps.BlackQueenRook ^ BLACK_ROOKS ^ ((long)1 << 59);
    	UpdateComboBoards();
    	return this;
    }
    public Bitboards undoBlackKS(){
    	BLACK_KING = BLACK_KING >> 2;
    	BLACK_ROOKS = MoveMaps.BlackKingRook ^ BLACK_ROOKS ^ ((long)1 << 61);
    	UpdateComboBoards();
    	return this;
    }
    
    public Bitboards makeNormalMove(Move myMove) {
		
		long from = myMove.getFrom();
		long to   = myMove.getTo();
		
		long fromBoard = Long.decode("0x1") << from;
		long toBoard = Long.decode("0x1") << to;
	
		long temp;
		
		long[] BOARDS = {WHITE_PAWNS, WHITE_KNIGHTS, WHITE_BISHOPS, WHITE_ROOKS, WHITE_QUEENS, WHITE_KING, BLACK_PAWNS, BLACK_KNIGHTS, BLACK_BISHOPS, BLACK_ROOKS, BLACK_QUEENS, BLACK_KING};
		//int[] pieceCodes = {Move.pawn, Move.knight, Move.bishop, Move.rook, Move.queen, Move.king, Move.pawn, Move.knight, Move.bishop, Move.rook, Move.queen, Move.king};
		
		for(int i = 0; i < 12; i++){
			
			//if the board has piece where the move goes, 1
			//else, 0
			int took = Long.bitCount(BOARDS[i] & toBoard);
			
			//************* DIFFERENT FROM UNDO *****************************************
			//set the captured piece to the corresponding pieceCode for the current board
			myMove.setCapturedPiece((took*i) + myMove.getCaptured());
			
			//That board removes the '1' when present
			BOARDS[i] = BOARDS[i] ^ (BOARDS[i] & toBoard);
			
			//took = 1, Capture
			//took = 0, no Capture
			//myMove.setCapture(took);
			took = (took | took << 1 | took << 2 | took << 3) & i;
			myMove.setCapturedPiece(took);
			
			//If the board has a piece where our move STARTS
			temp = BOARDS[i] & fromBoard;
			
			//Remove the piece from that spot (if 0, does nothing)
			BOARDS[i] = BOARDS[i] ^ (temp);
			
			//If temp has a bit (piece started here), add the ToBoard bit
			BOARDS[i] = BOARDS[i] | ((long)Long.bitCount(temp) << to);
			
			//TestGame.printBitBoardForward(BOARDS[i]);
		}
		
		WHITE_PAWNS = BOARDS[0];
		WHITE_KNIGHTS = BOARDS[1];
		WHITE_BISHOPS = BOARDS[2];
		WHITE_ROOKS = BOARDS[3];
		WHITE_QUEENS = BOARDS[4];
		WHITE_KING = BOARDS[5];
		
		BLACK_PAWNS = BOARDS[6];
		BLACK_KNIGHTS = BOARDS[7];
		BLACK_BISHOPS = BOARDS[8];
		BLACK_ROOKS = BOARDS[9];
		BLACK_QUEENS = BOARDS[10];
		BLACK_KING = BOARDS[11];
		
		UpdateComboBoards();
		
		return this;
	}
    public Bitboards undoNormalMove(Move myMove) {
    	
    	long from = myMove.getTo();
		long to   = myMove.getFrom();
		
		long fromBoard = (long)1 << from;
		//long toBoard = Long.decode("0x1") << to;
	
		long temp;
		
		long[] BOARDS = {WHITE_PAWNS, WHITE_KNIGHTS, WHITE_BISHOPS, WHITE_ROOKS, WHITE_QUEENS, WHITE_KING, BLACK_PAWNS, BLACK_KNIGHTS, BLACK_BISHOPS, BLACK_ROOKS, BLACK_QUEENS, BLACK_KING};
		//int[] pieceCodes = {Move.pawn, Move.knight, Move.bishop, Move.rook, Move.queen, Move.king, Move.pawn, Move.knight, Move.bishop, Move.rook, Move.queen, Move.king};
		
		for(int i = 0; i < 12; i++){
			//System.out.println("Before Undoing Board " + i);
			//TestGame.printBitBoardForward(BOARDS[i]);
			
			//If the board has a piece where our move STARTS
			temp = BOARDS[i] & fromBoard;
			
			//Remove the piece from that spot (if 0, does nothing)
			BOARDS[i] = BOARDS[i] ^ (temp);
			
			//If temp has a bit (piece started here), add the ToBoard bit
			BOARDS[i] = BOARDS[i] | ((temp >> from) << to);
			
			if(i == myMove.getCaptured() && myMove.getTakes() == 1){
				BOARDS[i] = BOARDS[i] | fromBoard;
			}
			//System.out.println("After Undoing Board " + i);
			//TestGame.printBitBoardForward(BOARDS[i]);
		}
		
		WHITE_PAWNS = BOARDS[0];
		WHITE_KNIGHTS = BOARDS[1];
		WHITE_BISHOPS = BOARDS[2];
		WHITE_ROOKS = BOARDS[3];
		WHITE_QUEENS = BOARDS[4];
		WHITE_KING = BOARDS[5];
		
		BLACK_PAWNS = BOARDS[6];
		BLACK_KNIGHTS = BOARDS[7];
		BLACK_BISHOPS = BOARDS[8];
		BLACK_ROOKS = BOARDS[9];
		BLACK_QUEENS = BOARDS[10];
		BLACK_KING = BOARDS[11];
		
		UpdateComboBoards();
		
		return this;
    }
}

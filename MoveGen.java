package game2;
import java.util.ArrayList;

public class MoveGen {
	
    //private MoveMaps MoveMaps;
    private Position myPosition;
    private ArrayList<Move> myGenMoves;
    
    public void GenerateBlack(){
    	//System.out.println("in GenerateBlack()");
    	
    	//PAWN MOVES
    	reapPawns(myPosition.myBitboards.BLACK_PAWNS, MoveMaps.getBlPawn_Attacks(), MoveMaps.getBlPawn_Moves(), myPosition.myBitboards.WHITE_PIECES, BLACK);
    	
    	//KING MOVES
    	reapAll(myPosition.myBitboards.BLACK_KING, MoveMaps.getKing_Attacks(), myPosition.myBitboards.WHITE_PIECES, Move.king);
    	    	
    	//KNIGHT
    	reapAll(myPosition.myBitboards.BLACK_KNIGHTS, MoveMaps.getKnight_Attacks(), myPosition.myBitboards.WHITE_PIECES, Move.knight);
    	
    	//ROOK
    	rookAll(myPosition.myBitboards.BLACK_ROOKS, myPosition.myBitboards.WHITE_PIECES, false);
    	
    	//BISHOP
    	bishopAll(myPosition.myBitboards.BLACK_BISHOPS, myPosition.myBitboards.WHITE_PIECES, false);
    	
    	//QUEEN = ROOK + BISHOP
    	rookAll(myPosition.myBitboards.BLACK_QUEENS, myPosition.myBitboards.WHITE_PIECES, true);
    	bishopAll(myPosition.myBitboards.BLACK_QUEENS, myPosition.myBitboards.WHITE_PIECES, true);
    	
    	//System.out.println("In MoveGen.BlackCastles()");
    	//CASTLING
    	//BlackCastles();
    	
    	//System.out.println("In MoveGen.BlackEnPassent()");
    	//EN PASSENT
    	BlackEnPassent();
    	
    	//System.out.println("out of GenerateBlack()");
    }
    
    public void BlackEnPassent(){
    	
    	long blackPAWNS = myPosition.myBitboards.BLACK_PAWNS;
    	int enPass = myPosition.myGameInfo.getEnPassent();
    	
    	long EPpawn = (long)1 << enPass;
    	
    	while(Long.bitCount(blackPAWNS) > 0){
    		
    		int trail = Long.numberOfTrailingZeros(blackPAWNS);
    		
    		if(Long.bitCount(EPpawn & MoveMaps.getBlPawn_Attacks()[trail]) == 1){
    			int threats = Long.bitCount(MoveMaps.getBlPawn_Attacks()[enPass] & myPosition.myBitboards.WHITE_PIECES);
    			Move EP = new Move(Move.pawn | Move.takes | Move.threats(threats), trail, enPass);
    			myGenMoves.add(EP);
    		}
    		
    		blackPAWNS = blackPAWNS ^ ((long)1 << trail);
    	}
    }
    
    public void BlackCastles(){
    	
    	// K i N g S i D e
    	//****************
    	long ks = Long.bitCount(myPosition.myBitboards.EMPTY_SQUARES & MoveMaps.BlackKCastle);
    	ks = ks >> 1;
    	ks = ks & myPosition.myGameInfo.getBitNumber(GameInfo.BlackKingStillAt);
    	ks = ks & myPosition.myGameInfo.getBitNumber(GameInfo.BlackKingRookStillAt);
    	//ks = (ks << 61) | (ks << 62);
    	//if bitCount was 0, 1 | King Moved | Kings Rook Moved		KS is now 0
    	//if bitcount was 2, and nothing moved						KS is now 1         MoveMaps.BlackKCastle
    	
    	if(ks == 1){
    		//CheckTest for each bit in KS
	    	long ksAttacked = MoveMaps.BlackKCastle;
	    	
	    	while(Long.bitCount(ksAttacked) > 0){
	    		int i = Long.numberOfTrailingZeros(ksAttacked);
	    		if(Long.bitCount(myPosition.checkTestBlackSquare(i)) != 0)
	    			break;
	    		else
	    			ksAttacked = ksAttacked ^ ((long)1 << i);
	    	}
	    	//If no attacks on any squares
	    	if(Long.bitCount(ksAttacked) == 0){
	    		int threats = rookThreatens(61, myPosition.myBitboards.WHITE_PIECES);
	    		threats = threats + Long.bitCount(MoveMaps.getKing_Attacks()[62] & myPosition.myBitboards.WHITE_PIECES);
	    		Move KScastle = new Move(Move.ksCastle | Move.threats(threats), 0, 0);
	    		myGenMoves.add(KScastle);
	    	}
    	}
    	// Q u E e N s I d E
    	//******************
    	long qs = Long.bitCount(myPosition.myBitboards.EMPTY_SQUARES & MoveMaps.BlackQCastle);
    	qs = (qs & 1) & (qs >> 1);
    	qs = qs & myPosition.myGameInfo.getBitNumber(GameInfo.BlackKingStillAt);
    	qs = qs & myPosition.myGameInfo.getBitNumber(GameInfo.BlackQueenRookStillAt);
    	//qs = (qs << 57) | (qs << 58) | (qs << 59);
    	//if bitCount was 0, 1 | King Moved | Kings Rook Moved		qs is now 0
    	//if bitcount was 2, and nothing moved						qs is now 1		MoveMaps.BlackQCastle
    	
    	if(qs == 1){
    		//CheckTest for each bit in QS
	    	long qsAttacked = MoveMaps.BlackQCastle;
	    	
	    	while(Long.bitCount(qsAttacked) > 0){
	    		int i = Long.numberOfTrailingZeros(qsAttacked);
	    		if(Long.bitCount(myPosition.checkTestBlackSquare(i)) != 0)
	    			break;
	    		else
	    			qsAttacked = qsAttacked ^ ((long)1 << i);
	    	}
	    	//If no attacks on any squares
	    	if(Long.bitCount(qsAttacked) == 0){
	    		int threats = rookThreatens(59, myPosition.myBitboards.WHITE_PIECES);
	    		threats = threats + Long.bitCount(MoveMaps.getKing_Attacks()[58] & myPosition.myBitboards.WHITE_PIECES);
	    		Move QScastle = new Move(Move.qsCastle | Move.threats(threats), 0, 0);
	    		myGenMoves.add(QScastle);
	    	}
    	}
    	
    }
    
    public void GenerateWhite(){
    	//System.out.println("in GenerateWhite()");
    	
    	//PAWN MOVES
    	reapPawns(myPosition.myBitboards.WHITE_PAWNS, MoveMaps.getWhPawn_Attacks(), MoveMaps.getWhPawn_Moves(), myPosition.myBitboards.BLACK_PIECES, WHITE);
    	
    	//KING MOVES
    	reapAll(myPosition.myBitboards.WHITE_KING, MoveMaps.getKing_Attacks(), myPosition.myBitboards.BLACK_PIECES, Move.king);
    	
    	//KNIGHT
    	reapAll(myPosition.myBitboards.WHITE_KNIGHTS, MoveMaps.getKnight_Attacks(), myPosition.myBitboards.BLACK_PIECES, Move.knight);
    	
    	//ROOK
    	rookAll(myPosition.myBitboards.WHITE_ROOKS, myPosition.myBitboards.BLACK_PIECES, false);
    	
    	//BISHOP
    	bishopAll(myPosition.myBitboards.WHITE_BISHOPS, myPosition.myBitboards.BLACK_PIECES, false);
    	
    	//QUEEN = ROOK + BISHOP
    	rookAll(myPosition.myBitboards.WHITE_QUEENS, myPosition.myBitboards.BLACK_PIECES, true);
    	bishopAll(myPosition.myBitboards.WHITE_QUEENS, myPosition.myBitboards.BLACK_PIECES, true);
    	
    	//System.out.println("In MoveGen.WhiteCastles()");
    	//CASTLES
    	//WhiteCastles();
    	//System.out.println("In MoveGen.WhiteEnPassent()");
    	//EN PASSENT
    	WhiteEnPassent();
    	
    	//System.out.println("out of GenerateWhite()");
    }
    
    public void WhiteEnPassent(){
    	
    	long whitePAWNS = myPosition.myBitboards.WHITE_PAWNS;
    	int enPass = myPosition.myGameInfo.getEnPassent();
    	
    	long EPpawn = (long)1 << enPass;
    	
    	while(Long.bitCount(whitePAWNS) > 0){
    		
    		int trail = Long.numberOfTrailingZeros(whitePAWNS);
    		
    		if(Long.bitCount(EPpawn & MoveMaps.getWhPawn_Attacks()[trail]) == 1){
    			int threats = Long.bitCount(MoveMaps.getWhPawn_Attacks()[enPass] & myPosition.myBitboards.BLACK_PIECES);
    			Move EP = new Move(Move.pawn | Move.takes | Move.threats(threats), trail, enPass);
    			myGenMoves.add(EP);
    		}
    		
    		whitePAWNS = whitePAWNS ^ ((long)1 << trail);
    	}
    }
    
    public void WhiteCastles(){
    	
    	// K i N g S i D e
    	//****************
    	long ks = Long.bitCount(myPosition.myBitboards.EMPTY_SQUARES & MoveMaps.WhiteKCastle);
    	//System.out.println("***ks 				= " + Long.toBinaryString(ks));
    	ks = ks >> 1;
    	//System.out.println("***ks 				= " + Long.toBinaryString(ks));
    	
    	//System.out.println("***WhiteKingStillAt = " + Long.toBinaryString(myPosition.myGameInfo.getBitNumber(GameInfo.WhiteKingStillAt)));
    	ks = ks & myPosition.myGameInfo.getBitNumber(GameInfo.WhiteKingStillAt);
    	
    	//System.out.println("***ks = " + Long.toBinaryString(ks));
    	//System.out.println("***WhiteKingookStillAt = " + Long.toBinaryString(myPosition.myGameInfo.getBitNumber(GameInfo.WhiteKingStillAt)));
    	ks = ks & myPosition.myGameInfo.getBitNumber(GameInfo.WhiteKingRookStillAt);
    	//System.out.println("***ks = " + Long.toBinaryString(ks));
    	
    	//ks = (ks << 61) | (ks << 62);
    	//if bitCount was 0, 1 | King Moved | Kings Rook Moved		KS is now 0
    	//if bitcount was 2, and nothing moved						KS is now 1         MoveMaps.WhiteKCastle
    	
    	if(ks == 1){
    		//CheckTest for each bit in KS
	    	long ksAttacked = MoveMaps.WhiteKCastle;
	    	
	    	while(Long.bitCount(ksAttacked) > 0){
	    		int i = Long.numberOfTrailingZeros(ksAttacked);
	    		if(Long.bitCount(myPosition.checkTestWhiteSquare(i)) != 0)
	    			break;
	    		else
	    			ksAttacked = ksAttacked ^ ((long)1 << i);
	    	}
	    	//If no attacks on any squares
	    	if(Long.bitCount(ksAttacked) == 0){
	    		int threats = rookThreatens(5, myPosition.myBitboards.BLACK_PIECES);
	    		threats = threats + Long.bitCount(MoveMaps.getKing_Attacks()[6] & myPosition.myBitboards.BLACK_PIECES);
	    		Move KScastle = new Move(Move.ksCastle | Move.threats(threats), 0, 0);
	    		myGenMoves.add(KScastle);
	    	}
    	}
    	// Q u E e N s I d E
    	//******************
    	long qs = Long.bitCount(myPosition.myBitboards.EMPTY_SQUARES & MoveMaps.WhiteQCastle);
    	qs = (qs & 1) & (qs >> 1);
    	qs = qs & myPosition.myGameInfo.getBitNumber(GameInfo.WhiteKingStillAt);
    	qs = qs & myPosition.myGameInfo.getBitNumber(GameInfo.WhiteQueenRookStillAt);
    	//qs = (qs << 57) | (qs << 58) | (qs << 59);
    	//if bitCount was 0, 1 | King Moved | Kings Rook Moved		qs is now 0
    	//if bitcount was 2, and nothing moved						qs is now 1		MoveMaps.WhiteQCastle
    	
    	if(qs == 1){
    		//CheckTest for each bit in QS
	    	long qsAttacked = MoveMaps.WhiteQCastle;
	    	
	    	while(Long.bitCount(qsAttacked) > 0){
	    		int i = Long.numberOfTrailingZeros(qsAttacked);
	    		if(Long.bitCount(myPosition.checkTestWhiteSquare(i)) != 0)
	    			break;
	    		else
	    			qsAttacked = qsAttacked ^ ((long)1 << i);
	    	}
	    	//If no attacks on any squares
	    	if(Long.bitCount(qsAttacked) == 0){
	    		int threats = rookThreatens(3, myPosition.myBitboards.BLACK_PIECES);
	    		threats = threats + Long.bitCount(MoveMaps.getKing_Attacks()[2] & myPosition.myBitboards.BLACK_PIECES);
	    		Move QScastle = new Move(Move.qsCastle | Move.threats(threats), 0, 0);
	    		myGenMoves.add(QScastle);
	    	}
    	}
    	
    }
    
    private void bishopAll(long BITBOARD, long ENEMY_BOARD, boolean Q){
    	
    	while(Long.bitCount(BITBOARD) > 0){
    		
    		int bitNum = Long.numberOfTrailingZeros(BITBOARD);
    		long UpRight;
    		long UpRightAttacks;
    		long UpRightMoves;
    		
    		UpRight = MoveMaps.getUpRight_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		UpRight = (UpRight << 9) | (UpRight << 18) | (UpRight << 27) | (UpRight << 36) | (UpRight << 45) | (UpRight << 54);
    		UpRight = UpRight & MoveMaps.getUpRight_Moves()[bitNum];
    		UpRight = UpRight ^ MoveMaps.getUpRight_Moves()[bitNum];
    		
    		UpRightAttacks = UpRight & ENEMY_BOARD;
    		UpRightMoves = UpRight & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long DownLeft;
    		long DownLeftAttacks;
    		long DownLeftMoves;
    		
    		DownLeft = MoveMaps.getDownLeft_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		DownLeft = (DownLeft >>> 9) | (DownLeft >>> 18) | (DownLeft >>> 27) | (DownLeft >>> 36) | (DownLeft >>> 45) | (DownLeft >>> 54);
    		DownLeft = DownLeft & MoveMaps.getDownLeft_Moves()[bitNum];
    		DownLeft = DownLeft ^ MoveMaps.getDownLeft_Moves()[bitNum];

    		DownLeftAttacks = DownLeft & ENEMY_BOARD;
    		DownLeftMoves = DownLeft & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long UpLeft;
    		long UpLeftAttacks;
    		long UpLeftMoves;
    		
    		UpLeft = MoveMaps.getUpLeft_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		UpLeft = (UpLeft << 7) | (UpLeft << 14) | (UpLeft << 21) | (UpLeft << 28) | (UpLeft << 35) | (UpLeft << 42);
    		UpLeft = UpLeft & MoveMaps.getUpLeft_Moves()[bitNum];
    		UpLeft = UpLeft ^ MoveMaps.getUpLeft_Moves()[bitNum];

    		UpLeftAttacks = UpLeft & ENEMY_BOARD;
    		UpLeftMoves = UpLeft & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long DownRight;
    		long DownRightAttacks;
    		long DownRightMoves;
    		
    		DownRight = MoveMaps.getDownRight_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		DownRight = (DownRight >>> 7) | (DownRight >>> 14) | (DownRight >>> 21) | (DownRight >>> 28) | (DownRight >>> 35) | (DownRight >>> 42);
    		DownRight = DownRight & MoveMaps.getDownRight_Moves()[bitNum];
    		DownRight = DownRight ^ MoveMaps.getDownRight_Moves()[bitNum];

    		DownRightAttacks = DownRight & ENEMY_BOARD;
    		DownRightMoves = DownRight & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long AllAttacks = UpRightAttacks | DownLeftAttacks | UpLeftAttacks | DownRightAttacks;
    		long AllMoves 	= UpRightMoves | DownLeftMoves | UpLeftMoves | DownRightMoves;
    		
    		reapBishopAttacks(AllAttacks, bitNum, ENEMY_BOARD, Q);
    		reapBishopMoves(AllMoves, bitNum, ENEMY_BOARD, Q);
			
    		BITBOARD = BITBOARD ^ ((long)1 << bitNum);
    	}

    }
    
	    private void reapBishopAttacks(long ATTACKS, int fromBit, long ENEMY_BOARD, boolean Queen){
	    	
	    	while(Long.bitCount(ATTACKS) > 0){
	    		
	    		int toBit = Long.numberOfTrailingZeros(ATTACKS);
	    		int threatNum = bishopThreatens(toBit, ENEMY_BOARD);
	    		Move myAttack;
	    		if(Queen){
	    			threatNum = threatNum + rookThreatens(toBit, ENEMY_BOARD);
	    			myAttack = new Move(Move.queen | Move.takes | Move.threats(threatNum), fromBit, toBit);
	    		}
	    		else{
	    			myAttack = new Move(Move.bishop | Move.takes | Move.threats(threatNum), fromBit, toBit);	
	    		}
	    		myGenMoves.add(myAttack);
	    		
	    		ATTACKS = ATTACKS ^ ((long)1 << toBit);
	    	}
	    }
	    private void reapBishopMoves(long MOVES, int fromBit, long ENEMY_BOARD, boolean Queen){
	    	
	    	while(Long.bitCount(MOVES) > 0){
	    		
	    		int toBit = Long.numberOfTrailingZeros(MOVES);
	    		int threatNum = bishopThreatens(toBit, ENEMY_BOARD);
	    		Move myMove;
	    		if(Queen){
	    			threatNum = threatNum + rookThreatens(toBit, ENEMY_BOARD);
	    			myMove = new Move(Move.queen | Move.threats(threatNum), fromBit, toBit);	
	    		}
	    		else{
	    			myMove = new Move(Move.bishop | Move.threats(threatNum), fromBit, toBit);	
	    		}
	    		myGenMoves.add(myMove);
	    		
	    		MOVES = MOVES ^ ((long)1 << toBit);
	    	}
	    }

		    private int bishopThreatens(int bitNum, long ENEMY_BOARD){
			    
				long UpRight = MoveMaps.getUpRight_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				UpRight = (UpRight << 9) | (UpRight << 18) | (UpRight << 27) | (UpRight << 36) | (UpRight << 45) | (UpRight << 54);
				UpRight = UpRight & MoveMaps.getUpRight_Moves()[bitNum];
				UpRight = UpRight ^ MoveMaps.getUpRight_Moves()[bitNum];
				
				UpRight = UpRight & ENEMY_BOARD;
				
				long DownLeft = MoveMaps.getDownLeft_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				DownLeft = (DownLeft << 9) | (DownLeft << 18) | (DownLeft << 27) | (DownLeft << 36) | (DownLeft << 45) | (DownLeft << 54);
				DownLeft = DownLeft & MoveMaps.getDownLeft_Moves()[bitNum];
				DownLeft = DownLeft ^ MoveMaps.getDownLeft_Moves()[bitNum];
		
				DownLeft = DownLeft & ENEMY_BOARD;
				
				long UpLeft = MoveMaps.getUpLeft_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				UpLeft = (UpLeft << 7) | (UpLeft << 14) | (UpLeft << 21) | (UpLeft << 28) | (UpLeft << 35) | (UpLeft << 42);
				UpLeft = UpLeft & MoveMaps.getUpLeft_Moves()[bitNum];
				UpLeft = UpLeft ^ MoveMaps.getUpLeft_Moves()[bitNum];
		
				UpLeft = UpLeft & ENEMY_BOARD;
				
				long DownRight = MoveMaps.getDownRight_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				DownRight = (DownRight << 7) | (DownRight << 14) | (DownRight << 21) | (DownRight << 28) | (DownRight << 35) | (DownRight << 42);
				DownRight = DownRight & MoveMaps.getDownRight_Moves()[bitNum];
				DownRight = DownRight ^ MoveMaps.getDownRight_Moves()[bitNum];
		
				DownRight = DownRight & ENEMY_BOARD;
		
				return Long.bitCount(UpRight|DownLeft|UpLeft|DownRight);
		    }
 
    private void rookAll(long BITBOARD, long ENEMY_BOARD, boolean Q){
    	
    	while(Long.bitCount(BITBOARD) > 0){
    		
    		int bitNum = Long.numberOfTrailingZeros(BITBOARD);
    		long right;
    		long rightAttacks;
    		long rightMoves;
    		
    		right = MoveMaps.getRight_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		right = (right << 1) | (right << 2) | (right << 3) | (right << 4) | (right << 5) | (right << 6);
    		right = right & MoveMaps.getRight_Moves()[bitNum];
    		right = right ^ MoveMaps.getRight_Moves()[bitNum];
    		
    		rightAttacks = right & ENEMY_BOARD;
    		rightMoves = right & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long left;
    		long leftAttacks;
    		long leftMoves;
    		
    		left = MoveMaps.getLeft_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		left = (left >>> 1) | (left >>> 2) | (left >>> 3) | (left >>> 4) | (left >>> 5) | (left >>> 6);
    		left = left & MoveMaps.getLeft_Moves()[bitNum];
    		left = left ^ MoveMaps.getLeft_Moves()[bitNum];

    		leftAttacks = left & ENEMY_BOARD;
    		leftMoves = left & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long up;
    		long upAttacks;
    		long upMoves;
    		
    		up = MoveMaps.getUp_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		up = (up << 8) | (up << 16) | (up << 24) | (up << 32) | (up << 40) | (up << 48);
    		up = up & MoveMaps.getUp_Moves()[bitNum];
    		up = up ^ MoveMaps.getUp_Moves()[bitNum];

    		upAttacks = up & ENEMY_BOARD;
    		upMoves = up & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long down;
    		long downAttacks;
    		long downMoves;
    		
    		down = MoveMaps.getDown_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
    		down = (down >>> 8) | (down >>> 16) | (down >>> 24) | (down >>> 32) | (down >>> 40) | (down >>> 48);
    		down = down & MoveMaps.getDown_Moves()[bitNum];
    		down = down ^ MoveMaps.getDown_Moves()[bitNum];

    		downAttacks = down & ENEMY_BOARD;
    		downMoves = down & myPosition.myBitboards.EMPTY_SQUARES;
    		
    		long AllAttacks = upAttacks | downAttacks | rightAttacks | leftAttacks;
    		long AllMoves 	= upMoves | downMoves | rightMoves | leftMoves;
    		
    		reapRookAttacks(AllAttacks, bitNum, ENEMY_BOARD, Q);
    		reapRookMoves(AllMoves, bitNum, ENEMY_BOARD, Q);
    		
    		BITBOARD = BITBOARD ^ ((long)1 << bitNum);
    	}

    }
    
	    private void reapRookAttacks(long ATTACKS, int fromBit, long ENEMY_BOARD, boolean Queen){
	    	
	    	while(Long.bitCount(ATTACKS) > 0){
	    		
	    		int toBit = Long.numberOfTrailingZeros(ATTACKS);
	    		int threatNum = rookThreatens(toBit, ENEMY_BOARD);
	    		Move myAttack;
	    		if(Queen){
	    			threatNum = threatNum + bishopThreatens(toBit, ENEMY_BOARD);
	    			myAttack = new Move(Move.queen | Move.takes | Move.threats(threatNum), fromBit, toBit);
	    		}
	    		else{
	    			myAttack = new Move(Move.rook | Move.takes | Move.threats(threatNum), fromBit, toBit);	
	    		}
	    		//if(!(myPosition.myBitboards.makeMove(myAttack).checkTest(myMaps, myPosition.myGameInfo.getCheckMove(GameInfo.WhiteTurn))))
	    			myGenMoves.add(myAttack);
	    		
	    		ATTACKS = ATTACKS ^ ((long)1 << toBit);
	    	}
	    }
	    
	    private void reapRookMoves(long MOVES, int fromBit, long ENEMY_BOARD, boolean Queen){
	    	;
	    	while(Long.bitCount(MOVES) > 0){
	    		
	    		int toBit = Long.numberOfTrailingZeros(MOVES);
	    		int threatNum = rookThreatens(toBit, ENEMY_BOARD);
	    		Move myMove;
	    		if(Queen){
	    			threatNum = threatNum + bishopThreatens(toBit, ENEMY_BOARD);
	    			myMove = new Move(Move.queen | Move.threats(threatNum), fromBit, toBit);	
	    		}
	    		else{
	    			myMove = new Move(Move.rook | Move.threats(threatNum), fromBit, toBit);	
	    		}
	    		//if(!(myPosition.myBitboards.makeMove(myAttack).checkTest(myMaps, myPosition.myGameInfo.getCheckMove(GameInfo.WhiteTurn))))
	    			myGenMoves.add(myMove);
	    		
	    		MOVES = MOVES ^ ((long)1 << toBit);
	    	}
	    }

		    private int rookThreatens(int bitNum, long ENEMY_BOARD){
		    
				long right = MoveMaps.getRight_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				right = (right << 1) | (right << 2) | (right << 3) | (right << 4) | (right << 5) | (right << 6);
				right = right & MoveMaps.getRight_Moves()[bitNum];
				right = right ^ MoveMaps.getRight_Moves()[bitNum];
				right = right & ENEMY_BOARD;
				
				long left = MoveMaps.getLeft_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				left = (left >>> 1) | (left >>> 2) | (left >>> 3) | (left >>> 4) | (left >>> 5) | (left >>> 6);
				left = left & MoveMaps.getLeft_Moves()[bitNum];
				left = left ^ MoveMaps.getLeft_Moves()[bitNum];
				left = left & ENEMY_BOARD;
				
				long up = MoveMaps.getUp_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				up = (up << 8) | (up << 16) | (up << 24) | (up << 32) | (up << 40) | (up << 48);
				up = up & MoveMaps.getUp_Moves()[bitNum];
				up = up ^ MoveMaps.getUp_Moves()[bitNum];
				up = up & ENEMY_BOARD;
				
				long down = MoveMaps.getDown_Moves()[bitNum] & myPosition.myBitboards.ALL_PIECES;
				down = (down >>> 8) | (down >>> 16) | (down >>> 24) | (down >>> 32) | (down >>> 40) | (down >>> 48);
				down = down & MoveMaps.getDown_Moves()[bitNum];
				down = down ^ MoveMaps.getDown_Moves()[bitNum];
				down = down & ENEMY_BOARD;
		
				return Long.bitCount(right|left|up|down);
		    }
        
    private void reapAll(long BITBOARD, long[] MotionMap, long ENEMY_BOARD, byte piece){
    	
    	while(Long.bitCount(BITBOARD) > 0){
	    	
    		int toBit	= Long.numberOfTrailingZeros(BITBOARD);
	    	long Attacks 	= MotionMap[toBit] & ENEMY_BOARD;
	    	long Moves 		= MotionMap[toBit] & myPosition.myBitboards.EMPTY_SQUARES;
	    	
	    	reapAttack(Attacks, MotionMap, ENEMY_BOARD, toBit, piece);
	    	reapMove(Moves, MotionMap, ENEMY_BOARD, toBit, piece);
	    	
	    	BITBOARD = BITBOARD ^ ((long)1 << toBit);
    	}
    }
    
    private void reapPawns(long PAWNBOARD, long[] AttackMap, long[] MotionMap, long ENEMY_BOARD, int isWHITE){

    	long TEMPPAWNS = PAWNBOARD;
    	
    	while(Long.bitCount(TEMPPAWNS) > 0){
    		
    		int CurrentBit = Long.numberOfTrailingZeros(TEMPPAWNS);
    		long pawnAttacks = AttackMap[CurrentBit] & ENEMY_BOARD;
    		
    		reapAttack(pawnAttacks, AttackMap, ENEMY_BOARD, CurrentBit, Move.pawn);
    		TEMPPAWNS = TEMPPAWNS ^ ((long)1 << CurrentBit);
    	}
    	
    	//Moving
    	long pawnMoves;
    	if(isWHITE > 0)
    		pawnMoves = (PAWNBOARD << 8) & myPosition.myBitboards.EMPTY_SQUARES;
    	else
    		pawnMoves = (PAWNBOARD >> 8) & myPosition.myBitboards.EMPTY_SQUARES;

    	reapPawnMoves(pawnMoves, AttackMap, ENEMY_BOARD, isWHITE);
    }
    
    
    	private void reapAttack(long BITBOARD, long[] MotionMap, long ENEMY_BOARD, int Origin, byte piece){
    	
			while(Long.bitCount(BITBOARD) > 0){
				
	    		int toBit = Long.numberOfTrailingZeros(BITBOARD);
	    		int threatNum = Long.bitCount(MotionMap[toBit] & ENEMY_BOARD);
	
	    		Move Attack = new Move(piece | Move.takes | Move.threats(threatNum), Origin, toBit);
	    		
	    		//if(!(myPosition.myBitboards.makeMove(Attack).checkTest(myMaps, myPosition.myGameInfo.getCheckMove(GameInfo.WhiteTurn))))
	    			myGenMoves.add(Attack);
	        	
	        	BITBOARD = BITBOARD ^ ((long)1 << toBit);
			}
	    }
    
    	private void reapMove(long BITBOARD, long[] MotionMap, long ENEMY_BOARD, int Origin, byte piece){
    		
    		while(Long.bitCount(BITBOARD) > 0){
				
	    		int toBit = Long.numberOfTrailingZeros(BITBOARD);
	    		int threatNum = Long.bitCount(MotionMap[toBit] & ENEMY_BOARD);
	    		
	    		Move Attack = new Move(piece | Move.threats(threatNum), Origin, toBit);
	    		//if(!(myPosition.myBitboards.makeMove(Attack).checkTest(myMaps, myPosition.myGameInfo.getCheckMove(GameInfo.WhiteTurn))))
		    		myGenMoves.add(Attack);
	    		
	        	BITBOARD = BITBOARD ^ ((long)1 << toBit);
			}
	    }
      
    	private void reapPawnMoves(long MOVES, long[] AttackMap, long ENEMY_BOARD, int isWHITE){
    		
    		//DOUBLES = the normal moves that end on the third row, alone, shifted up again, ANDed with empties
    		long DOUBLES;
    		if(isWHITE > 0)
	    		DOUBLES = ((MOVES & 0xFF0000) << 8) & myPosition.myBitboards.EMPTY_SQUARES;
	    	else
	    		DOUBLES = ((MOVES & Long.decode("0x0000FF0000000000")) >>> 8) & myPosition.myBitboards.EMPTY_SQUARES;
	    	//Single Moves
	    	while(Long.bitCount(MOVES) > 0){
		
	    		int toBit = Long.numberOfTrailingZeros(MOVES);
	    		int threatNum = Long.bitCount(AttackMap[toBit] & ENEMY_BOARD);
	    		
	    		Move myMove = new Move(Move.pawn | Move.threats(threatNum), toBit-(8*isWHITE), toBit);
	    		//if(!(myPosition.myBitboards.makeMove(myMove).checkTest(myMaps, myPosition.myGameInfo.getCheckMove(GameInfo.WhiteTurn))))
		    		myGenMoves.add(myMove);
	        	MOVES = MOVES ^ ((long)1 << toBit);
	    	}
	    	
	    	//Double Moves
	    	while(Long.bitCount(DOUBLES) > 0){

	    		int toBit = Long.numberOfTrailingZeros(DOUBLES);
	    		int threatNum = Long.bitCount(AttackMap[toBit] & ENEMY_BOARD);
	    		
	    		Move myMove = new Move(Move.pawn | Move.threats(threatNum), toBit-(16*isWHITE), toBit);
	    		//if(!(myPosition.myBitboards.makeMove(myMove).checkTest(myMaps, myPosition.myGameInfo.getCheckMove(GameInfo.WhiteTurn))))
		    		myGenMoves.add(myMove);

	        	DOUBLES = DOUBLES ^ ((long)1 << toBit);
	    	}
	    }
 
    
    private static final int WHITE = 1;
    private static final int BLACK = -1;
    
    public void clearMoves(){
    	myGenMoves.clear();
    }
    public Position getPos(){
    	return myPosition;
    }
    MoveGen(Position p){
    	//System.out.println("in MoveGen(Position p)");
    	myPosition = p;
    	myGenMoves = new ArrayList<Move>();
    	//System.out.println("iout of MoveGen(Position p)");
    }
    
    MoveGen(){
    	//System.out.println("in MoveGen()");
    	myPosition = new Position();
    	myGenMoves = new ArrayList<Move>();
    	//System.out.println("in MoveGen()");
    }
    
    public ArrayList<Move> getMoves(){
    	return myGenMoves;
    }
}

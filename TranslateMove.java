package game2;

public class TranslateMove {

	Move myMove;
	String myString;
	
	TranslateMove(String theCommand){
		myString = theCommand;
		encode();
	}
	TranslateMove(Move theMove){
		myMove = theMove;
		decode();
	}

	public int ConvertSquare(char a, char b){
		int row = b - 1;
		int col = a - 'a';
		return (row*8 + col);
	}
	
	public String ConvertSquare(long i){
		int row = (int) (i >>> 3);
		int col = (int) (i & 7);
		return ((char)(col+'a') + "" + (row+1));
	}
	
	public int char_int(char a){
		switch(a){
		case 'N': return Move.knight;
		case 'B': return Move.bishop;
		case 'R': return Move.rook;
		case 'Q': return Move.queen;
		case 'K': return Move.king;
		default: return 0;
		}
	}
	
	public char int_char(long d){
		int a = (int)d;
		switch(a){
		case Move.knight: return 'N';
		case Move.bishop: return 'B';
		case Move.rook: return 'R';
		case Move.queen: return 'Q';
		case Move.king: return 'K';
		default: return 0;
		}
	}
	
	public void encode(){
		//use the string S to update move M
		
		if(myString == "0-0-0")
			myMove = new Move(Move.qsCastle, 0, 0);

		if(myString == "0-0")
			myMove = new Move(Move.ksCastle, 0, 0);

		if(myString.length() == 2){
			//e4...
			int to = ConvertSquare(myString.charAt(myString.length()-2), myString.charAt(myString.length()-1));
			myMove = new Move(Move.pawn, 0, to);
		}
		
		if(myString.length() == 3){
			//Nf6...
			int to = ConvertSquare(myString.charAt(myString.length()-2), myString.charAt(myString.length()-1));
			int piece = char_int(myString.charAt(0));
			myMove = new Move(piece, 0, to);
		}
		
		if(myString.length() == 4){
			//e4e5
			int to = ConvertSquare(myString.charAt(myString.length()-2), myString.charAt(myString.length()-1));
			int from = ConvertSquare(myString.charAt(myString.length()-4), myString.charAt(myString.length()-3));
			myMove = new Move(Move.pawn, from, to);
		}
		
		if(myString.length() == 5){
			int to = ConvertSquare(myString.charAt(myString.length()-2), myString.charAt(myString.length()-1));
			int from = ConvertSquare(myString.charAt(myString.length()-4), myString.charAt(myString.length()-3));
			int piece = int_char(myString.charAt(0));
			myMove = new Move(piece, from, to);
		}
	}
	
	public void decode(){
		//use move MyMove to generate the string S
		myString ="";
		if(int_char(myMove.getRank()) > 0)
			myString += int_char(myMove.getRank());
		if(myMove.getTakes() == 1)
			myString += "" + ConvertSquare(myMove.getFrom()) + "x" + ConvertSquare(myMove.getTo());
		else
			myString += "" + ConvertSquare(myMove.getFrom()) + "" + ConvertSquare(myMove.getTo());
	}
}

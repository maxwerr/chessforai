package game2;

public class GameInfo {

	private int myData;
	
	GameInfo(GameInfo myInfo) {
		myData = myInfo.myData;
	}
	GameInfo() {
		System.out.println("In GameInfo()");
		myData = WhiteTurn | BlackKingStill | BlackKingRookStill | BlackQueenRookStill | WhiteKingStill | WhiteKingRookStill | WhiteQueenRookStill;
		System.out.println("Out of GameInfo()");
	}
	
	/*													  KRook	 	  KRook
	 * 	 					EnPassent Square  exists?	King  QRook\King  QRook	 (havent moved)
	 * 				WhTurn	|				 \		|   |			|
	 * 					|	|				  \		|	black		white
	 * 					|	|__________________\_	|   |___________|________
	 * getCheckMove(	13	12	11	10	9	8	7	6	5	4	3	2	1	0 )
	 * 
	 * 
	 */
	
	public String displayInfo(int i){
		
		String myString = Integer.toBinaryString(i);
		
		while(myString.length() < 14)
			myString = "0" + myString;
		
		return myString;
	}
	
	public void displayInfo(){
		System.out.println(displayInfo(myData));
	}
	
	// CONSTRUCTION
	//
	// Bit Numbers for each part of INFO
	public static final int WhiteTurnAt = 13;
	public static final int EnPassentAt = 7;
	public static final int EnPassentExistsAt = 6;
	public static final int BlackKingStillAt = 5;
	public static final int BlackKingRookStillAt = 4;
	public static final int BlackQueenRookStillAt = 3;
	public static final int WhiteKingStillAt = 2;
	public static final int WhiteKingRookStillAt = 1;
	public static final int WhiteQueenRookStillAt = 0;
	
	// USAGE
	//
	// each a (0* 1 0*) based on the constuction
	public static final int WhiteTurn = 1 << WhiteTurnAt;
	public static final int EnPassentExists = 1 << EnPassentExistsAt;
	public static final int BlackKingStill = 1 << BlackKingStillAt;
	public static final int BlackKingRookStill = 1 << BlackKingRookStillAt;
	public static final int BlackQueenRookStill = 1 << BlackQueenRookStillAt;
	public static final int WhiteKingStill = 1 << WhiteKingStillAt;
	public static final int WhiteKingRookStill = 1 << WhiteKingRookStillAt;
	public static final int WhiteQueenRookStill = 1 << WhiteQueenRookStillAt;
 
	public static final int FullData 	= Integer.parseInt("11111111111111", 2);
	public static final int FullEP 		= Integer.parseInt("01111110000000", 2);
	
	public int getBitNumber(int i){
		return (myData & (1 << i)) >>> i;
	}	
	public int getEnPassent(){
		return (myData >> EnPassentAt) & 63;
	}
	public int getMyData(){
		return myData;
	}
	
	public void makeMove(Move m) {
		castlersMove(m);
		enPassantMove(m);
		
		myData = myData ^ WhiteTurn;
		
	}
	public void undoMove(Move m) {
		myData = m.getGameState();
	}
	
	public void enPassantMove(Move m){
		//System.out.println("In enPassantMove()");
		
		int pawn = Integer.signum(m.getRank()) ^ 1;  // = 1 iFF pawn, else 0
		int jump = Long.bitCount((m.getFrom() ^ m.getTo()) >> 4);		// = 1 iFF a jump of two
	
		//System.out.println("Pawn = " + displayInfo(pawn));
		//System.out.println("Jump = " + displayInfo(jump));
		
		int enPassant = (pawn & jump) << EnPassentExistsAt;
		
		//System.out.println("EnPas= " + displayInfo(enPassant));
		//Sets ep bit in myData to ep bit in enPassant
		myData = (myData | enPassant) & ((FullData ^ (1 << EnPassentExistsAt)) ^ enPassant);
		
		//System.out.println("MyDat= " + displayInfo(myData));
		enPassant = Integer.bitCount(enPassant);
		//System.out.println("enPas= " + displayInfo(enPassant));
		enPassant = enPassant | (enPassant << 1) | (enPassant << 2) | (enPassant << 3) | (enPassant << 4) | (enPassant << 5);
		//System.out.println("or shift= " + displayInfo(enPassant));
		enPassant = (Math.min(m.getTo(), m.getFrom()) + 8) & enPassant;
		//System.out.println("min(to,from) + 8 = " + displayInfo(enPassant));
		enPassant = enPassant << EnPassentAt;
		//System.out.println("enPas << EPat= " + displayInfo(enPassant));
		
		myData = (myData | enPassant) & (FullData ^ FullEP ^ enPassant);
		//System.out.println("MyDat= " + displayInfo(myData));
	}
	
	public void castlersMove(Move m){
		//System.out.print("In CastlersMove()");
		
		int movesNow;
		long fromBoard = (long)1 << m.getFrom();
		
		movesNow = Long.bitCount(fromBoard & MoveMaps.BlackKing) << BlackKingStillAt;
		myData = (myData ^ movesNow) & myData;
		
		movesNow = Long.bitCount(fromBoard & MoveMaps.BlackKingRook) << BlackKingRookStillAt;
		myData = (myData ^ movesNow) & myData;
		
		movesNow = Long.bitCount(fromBoard & MoveMaps.BlackQueenRook) << BlackQueenRookStillAt;
		myData = (myData ^ movesNow) & myData;
		
		movesNow = Long.bitCount(fromBoard & MoveMaps.WhiteKing) << WhiteKingStillAt;
		myData = (myData ^ movesNow) & myData;
		
		movesNow = Long.bitCount(fromBoard & MoveMaps.WhiteKingRook) << WhiteKingRookStillAt;
		myData = (myData ^ movesNow) & myData;
		
		movesNow = Long.bitCount(fromBoard & MoveMaps.WhiteQueenRook) << WhiteQueenRookStillAt;
		myData = (myData ^ movesNow) & myData;
	}

}

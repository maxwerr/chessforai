package game2;

public class Move {
	
	Move(int p, int from, int to){
		//System.out.println("In Move(" + p + ", " + from + ", " + to + ")");
		placement = (from << fromBitAt) | (to << toBitAt);
		info = p;
	}
	public void setGameState(int g){
		gameState = g;
	}
	public int getGameState(){
		return gameState;
	}
	
	private int info;
	private int placement;
	private int gameState;
	
	/*			BIT PLACEMENT
	 * 			*************
	 * 
	 *	-	-	-   -	-   -	-   -	-   -	-   -
	 *	11  10  9   8   7   6   5   4   3   2   1   0
	 *	_____________________   _____________________
	 *	|						|
	 *	from bit				to bit
	 *	
	 *
	 *			PIECE INFO
	 *			**********
	 *
	 *		-	-	-   -	-   -	-   -	-   -	-   -
	 *		11	10  9   8   7   6   5   4   3   2   1   0
	 *		______________	_	_____________	_________
	 *				|		|		|				|	
	 *		capturedPiece	takes?	threatens	piece/cast
	 *
	 *
	 *			CARRIED GAME STATE
	 *			******************
	 *									  KRook	 	  KRook
	 * 		EnPassent Square  exists?	King  QRook\King  QRook	 (havent moved)
	 *  Turn|				 \		|   |			|
	 * 	|	|				  \		|	black		white
	 * 	|	|__________________\_	|   |___________|________
	 * 	13	12	11	10	9	8	7	6	5	4	3	2	1	0 )
	 * 
	 *													
	 */

	public void printMove(){
		
		String INFO = Integer.toBinaryString(info);
		while(INFO.length() < 12){
			INFO = "0" + INFO;
		}
		
		String GAMESTATE = Integer.toBinaryString(gameState);
		while(GAMESTATE.length() < 14){
			GAMESTATE = "0" + GAMESTATE;
		}
		
		System.out.print(INFO);
		System.out.print("\t\t" + GAMESTATE);
	}
	
	public static int threats(int i){		return (i << threatsAt);}
	
	//Locations of Move Parts
	private static final byte toBitAt = 0;
	private static final byte fromBitAt = 6;
	
	//Locations of PieceKind Parts
	private static final byte pieceRankAt = 0;
	private static final byte threatsAt = 3;
	private static final byte takesAt = 7;
	private static final byte capturedAt = 8;
	
	public static final int takes = 1 << takesAt;
	
	public int getFrom(){	return (placement >>> fromBitAt) & 63;}
	public int getTo(){		return (placement >>> toBitAt) & 63;}
	
	public int getRank(){		return (info >>> pieceRankAt) & 7;}
	public int getThreats(){	return (info >>> threatsAt) & 15;}
	public int getTakes(){		return (info >>> takesAt) & 1;}
	public int getCaptured(){	return (info >>> capturedAt) & 15;}
	
	public void setCapture(int i){
		info = info | (i << takesAt);
	}
	public void setCapturedPiece(int i){
		info = info | (i << capturedAt);
	}
	
	//PIECES CODES
	static final byte pawn = 0;					//	   000
	static final byte knight = 1;				//	   001
	static final byte bishop = 2;				//	   010
	static final byte rook = 3;					//	   011
	static final byte queen = 4;				//	   100
	static final byte king = 5;					//	   101
	static final byte ksCastle = 6;				//	   110
	static final byte qsCastle = 7;				//	   111

}

package game2;
import java.util.ArrayList;

public class AB {
	
	private int goalDepth;
	private Position pos;
	private MoveGen gen;
	private boolean MAX;
	private Move myBestMove;

	public Move getBestMove(){
		return myBestMove;
	}
	
	AB(int gd, Position p, boolean WHITE){
		MAX = WHITE;
		goalDepth = gd;
		pos = p;
		gen = new MoveGen(pos);
	}
	
	public void runner(){
		Move bestMove = null;
		int bestValue;
		ArrayList<Move> myMoves;
		
		if(MAX){
			bestValue = Integer.MIN_VALUE;
			gen.GenerateWhite();
			myMoves = gen.getMoves();
			
			for(int i = 0; i < myMoves.size(); i++){
				pos.makeMove(myMoves.get(i));
				int evalMin = evaluateMin(pos, Integer.MIN_VALUE, 1);
				if(evalMin > bestValue){
					bestValue = evalMin;
					bestMove = myMoves.get(i);
				}
				pos.undoMove(myMoves.get(i));
			}
		}
		else{
			bestValue = Integer.MAX_VALUE;
			gen.GenerateBlack();
			myMoves = gen.getMoves();
			
			for(int i = 0; i < myMoves.size(); i++){
				pos.makeMove(myMoves.get(i));
				int evalMax = evaluateMax(pos, Integer.MAX_VALUE, 1);
				if(evalMax > bestValue){
					bestValue = evalMax;
					bestMove = myMoves.get(i);
				}
				pos.undoMove(myMoves.get(i));
			}
		}
		myBestMove = bestMove;
	}
	
	
	public int evaluateMin(Position pos, int Beta, int currentDepth){
	    int Alpha = Integer.MAX_VALUE;
	    
	    //myGen.setPosition(pos);
	    gen.GenerateBlack();
	    
	    if (gen.getMoves().size() == 0 || currentDepth == goalDepth){
	    	if(MAX){
	    		return pos.ValueWhite;
	    	}
	    	else{
	    		return pos.ValueBlack;
	    	}
	    }
	    else{
	    	int size = gen.getMoves().size();
	        for(int i = 0; i < size; i++){
	        	pos.makeMove(gen.getMoves().get(i));
	        	int val = evaluateMax(pos,  Beta, currentDepth+1);
	        	Alpha = Math.max(Alpha, val);
	        	if (Alpha > Beta)
	        		break;
	        	pos.undoMove(gen.getMoves().get(i));
	        }
	    }
	    return Alpha;
    }
 
    public int evaluateMax(Position pos, int Beta, int currentDepth){
	    int Alpha = Integer.MIN_VALUE;
	    
	    //myGen.setPosition(pos);
	    gen.GenerateWhite();
	    
	    if (gen.getMoves().size() == 0 || currentDepth == goalDepth){
	    	if(MAX)
	    		return pos.ValueWhite;
	    	else
	    		return pos.ValueBlack;
	    }
	    else{ 
	    	int size = gen.getMoves().size();
	        for(int i = 0; i < size; i++){
	        	pos.makeMove(gen.getMoves().get(i));
	        	int val = evaluateMin(pos, Beta, currentDepth+1);
	        	Alpha = Math.min(Alpha, val);
	        	if (Alpha <= Beta)
	        		break;
	        	pos.undoMove(gen.getMoves().get(i));
	        }
	    }
	    return Alpha;
    }
}

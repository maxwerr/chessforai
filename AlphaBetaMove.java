package game2;

public class AlphaBetaMove {

	int depth;
	int value;
	Position pos;
	MoveGen myGen;
	
	boolean maxStarts;
	public static final boolean MAX = true;
	public static final boolean MIN = false;
	
	AlphaBetaMove(int depth, Position pos, MoveGen myGen, boolean starts){
		this.pos = pos;
		this.depth = depth;
		this.myGen = myGen;
		this.maxStarts = starts;
	}

	public void runner(){
		int value;
		if(maxStarts){
			value = evaluateMax(pos, (int)Double.NEGATIVE_INFINITY, 0);
		}
		else{
			value = evaluateMin(pos, (int)Double.POSITIVE_INFINITY, 0);
		}
		System.out.println("Value = " + value);
	}
	
    public int evaluateMin(Position pos, int Beta, int currentDepth){
	    int Alpha = (int)Double.POSITIVE_INFINITY;
	    
	    //myGen.setPosition(pos);
	    myGen.GenerateBlack();
	    
	    if (myGen.getMoves().size() == 0 || currentDepth == depth){
	    	if(maxStarts){
	    		return pos.ValueWhite;
	    	}
	    	else{
	    		return pos.ValueBlack;
	    	}
	    }
	    else{
	    	int size = myGen.getMoves().size();
	        for(int i = 0; i < size; i++){
	        	pos.makeMove(myGen.getMoves().get(i));
	        	int val = evaluateMax(pos,  Beta, currentDepth+1);
	        	Alpha = Math.max(Alpha, val);
	        	if (Alpha <= Beta)
	        		break;
	        	pos.undoMove(myGen.getMoves().get(i));
	        }
	    }
	    return Alpha;
    }
 
    public int evaluateMax(Position pos, int Beta, int currentDepth){
	    int Alpha = (int)Double.NEGATIVE_INFINITY;
	    
	    //myGen.setPosition(pos);
	    myGen.GenerateWhite();
	    
	    if (myGen.getMoves().size() == 0 || currentDepth == depth){
	    	if(maxStarts)
	    		return pos.ValueWhite;
	    	else
	    		return pos.ValueBlack;
	    }
	    else{ 
	    	int size = myGen.getMoves().size();
	        for(int i = 0; i < size; i++){
	        	pos.makeMove(myGen.getMoves().get(i));
	        	int val = evaluateMin(pos, Beta, currentDepth+1);
	        	Alpha = Math.min(Alpha, val);
	        	if (Alpha <= Beta)
	        		break;
	        	pos.undoMove(myGen.getMoves().get(i));
	        }
	    }
	    return Alpha;
    }
    
}
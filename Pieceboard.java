package game2;

public class Pieceboard {

	static final byte EMPTY 		= 0;
	
	static final byte WHITEPAWN 	= 1;
	static final byte WHITEROOK 	= 2;
	static final byte WHITEKNIGHT 	= 3;
	static final byte WHITEBISHOP 	= 4;
	static final byte WHITEQUEEN 	= 5;
	static final byte WHITEKING 	= 6;
	
	static final byte BLACKPAWN 	= -1;
	static final byte BLACKROOK 	= -2;
	static final byte BLACKKNIGHT 	= -3;
	static final byte BLACKBISHOP 	= -4;
	static final byte BLACKQUEEN 	= -5;
	static final byte BLACKKING 	= -6;
	
	Bitboards startBoards;
	
	public byte[][] Board = new byte[8][8];
	
	public void figureoutBitBoard(){
		
		reapPiece(startBoards.BLACK_BISHOPS, BLACKBISHOP);
		reapPiece(startBoards.BLACK_KNIGHTS, BLACKKNIGHT);
		reapPiece(startBoards.BLACK_ROOKS, BLACKROOK);
		reapPiece(startBoards.BLACK_PAWNS, BLACKPAWN);
		reapPiece(startBoards.BLACK_KING, BLACKKING);
		reapPiece(startBoards.BLACK_QUEENS, BLACKQUEEN);
		
		reapPiece(startBoards.WHITE_BISHOPS, WHITEBISHOP);
		reapPiece(startBoards.WHITE_KNIGHTS, WHITEKNIGHT);
		reapPiece(startBoards.WHITE_ROOKS, WHITEROOK);
		reapPiece(startBoards.WHITE_PAWNS, WHITEPAWN);
		reapPiece(startBoards.WHITE_KING, WHITEKING);
		reapPiece(startBoards.WHITE_QUEENS, WHITEQUEEN);
	}
	
	public void reapPiece(long bitBoard, byte piece){
		while(Long.bitCount(bitBoard) > 0){
			int first = Long.numberOfTrailingZeros(bitBoard);
			int col = (first & 7);
			int row = 7 - (first >>> 3);
			Board[row][col] = piece;
			bitBoard = bitBoard ^ ((long)1 << first);
		}
	}
	
	/*
	 * 	_	_	_	_	_	_	_	_
	 * 	56	57	58	59	60	61	62	63
	 * 	_	_	_	_	_	_	_	_
	 * 	48	49	50	51	52	53	54	55
	 * 	_	_	_	_	_	_	_	_
	 * 	40	41	42	43	44	45	46	47
	 * 	_	_	_	_	_	_	_	_
	 *  32	33	34	35	36	37	38	39
	 * 	_	_	_	_	_	_	_	_
	 * 	24	25	26	27	28	29	30	31
	 * 	_	_	_	_	_	_	_	_
	 * 	16	17	18	19	20	21	22	23
	 * 	_	_	_	_	_	_	_	_
	 * 	8	9	10	11	12	13	14	15
	 * 	_	_	_	_	_	_	_	_
	 * 	0	1	2	3	4	5	6	7
	 * 
	 */
	
	
	Pieceboard(Bitboards B){
		startBoards = B;
		figureoutBitBoard();
	}
	
	Pieceboard(){
		System.out.println("In PieceBoard()");
		
		for (int c = 0; c < 8; c++)
			for (int r = 2; r < 6; r++)
				Board[c][r] = EMPTY;
		for (int r = 0; r < 8; r++){
			Board[1][r] = BLACKPAWN;
			Board[6][r] = WHITEPAWN;
		}
		Board[0][0] = BLACKROOK;
		Board[7][0] = WHITEROOK;
		Board[0][1] = BLACKKNIGHT;
		Board[7][1] = WHITEKNIGHT;
		Board[0][2] = BLACKBISHOP;
		Board[7][2] = WHITEBISHOP;
		Board[0][3] = BLACKQUEEN;
		Board[7][3] = WHITEQUEEN;
		Board[0][4] = BLACKKING;
		Board[7][4] = WHITEKING;
		Board[0][5] = BLACKBISHOP;
		Board[7][5] = WHITEBISHOP;
		Board[0][6] = BLACKKNIGHT;
		Board[7][6] = WHITEKNIGHT;
		Board[0][7] = BLACKROOK;
		Board[7][7] = WHITEROOK;
		
		displayBoard();
	}
	
	public void displayBoard(){	
		for(int i = 0; i < 8; i++){
			for(int q = 0; q < 8; q++){
				switch(Board[i][q]){
				case EMPTY: System.out.print(" * "); break;
				
				case WHITEPAWN: 	System.out.print(" P "); break;
				case WHITEROOK: 	System.out.print(" R "); break;
				case WHITEKNIGHT: 	System.out.print(" N "); break;
				case WHITEBISHOP: 	System.out.print(" B "); break;
				case WHITEQUEEN: 	System.out.print(" Q "); break;
				case WHITEKING: 	System.out.print(" K "); break;
				
				case BLACKPAWN: 	System.out.print(" p "); break;
				case BLACKROOK: 	System.out.print(" r "); break;
				case BLACKKNIGHT: 	System.out.print(" n "); break;
				case BLACKBISHOP: 	System.out.print(" b "); break;
				case BLACKQUEEN: 	System.out.print(" q "); break;
				case BLACKKING: 	System.out.print(" k "); break;
				}
			}
			System.out.println();
		}
	}
	
}

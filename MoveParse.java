package game2;

class MoveParse{

	private String input;
	private String token;
	
	private int toBit;
	private int fromBit;
	private int rank;
	private boolean takes;
	

	public char lex(){

		char a = input.charAt(input.length()-1);
		if(a >= 'a' && a < 'i')
			return a;
		if(a >= '1' && a < 8)
			return a;
		if(a == 'x')
			return a;
		if(a == 'B' || a == 'K' || a == 'N' || a == 'R' || a == 'Q')
			return a;
		else{
			System.out.println("Sorry, we don't accept one OR more of those characters.  Thank you for not smoking.");
			return 'z';
		}
	}
	
	public char col(){
		char a = input.charAt(input.length()-1);
		if(a >= 'a' && a < 'i')
			return a;
		else
			return 'z';
	}
	public int row(){
		int a = input.charAt(input.length()-1);
		if(a >= 0 && a < 8)
			return a;
		else
			return 10;
	}
	public char rank(){
		char a = input.charAt(input.length()-1);
		if(a == 'B' || a == 'K' || a == 'N' || a == 'R' || a == 'Q')
			return a;
		else
			return 'z';
	}
	public boolean x(){
		char a = input.charAt(input.length()-1);
		if(a == 'x')
			return true;
		else
			return false;
	}
	
	public int square(){
		int i = col();
		int q = row();
		if(i != 'z' && q != 10)
			return (i*8)+8;
		else
			return 99;
	}
	public void removeLast(int i){
		input = input.substring(0, input.length()-i);
	}

	public void move(){

		toBit = square();
		
		if(toBit != 'z'){
			
			removeLast(2);
			takes = x();
			
			if(takes){
				
				removeLast(1);
				fromBit = square();
				
				if(fromBit != 'z'){
					
					removeLast(2);
					rank = rank();
					
					if(rank != 'z'){
			
						removeLast(1);
						
					}
					else{
						System.out.println("Invalid rank.");
					}
				}
				else{
					System.out.println("Invalid destination.");
				}
			}
			else{
				System.out.println("Invalid symbol.");
			}
		}
		else{
			System.out.println("Invalid starting square.");
		}
	}

}
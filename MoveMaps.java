package game2;

public class MoveMaps {

	private static long[] King_Attacks;
	private static long[] Knight_Attacks;
	private static long[] Bl_Pawn_Attacks;
	private static long[] Bl_Pawn_Moves;
	private static long[] Wh_Pawn_Attacks;
	private static long[] Wh_Pawn_Moves;
	private static long[] Right_Moves;
	private static long[] Left_Moves;
	private static long[] Up_Moves;
	private static long[] Down_Moves;
	private static long[] Up_Left_Moves;
	private static long[] Down_Left_Moves;
	private static long[] Up_Right_Moves;
	private static long[] Down_Right_Moves;
	static long BlackKing;
	static long BlackKingRook;
	static long BlackQueenRook;
	static long WhiteKing;
	static long WhiteKingRook;
	static long WhiteQueenRook;
	static long BlackKCastle;
	static long WhiteKCastle;
	static long BlackQCastle;
	static long WhiteQCastle;
	
								      						  //size  origin
	public static void makeMaps(){						      				  	  //r  c  r  c 	
		King_Attacks 	= getAttackBitboards(getKingMask(), 	3, 3, 1, 1);
		Knight_Attacks 	= getAttackBitboards(getKnightMask(), 	5, 5, 2, 2);
		Bl_Pawn_Attacks = getAttackBitboards(getBlPxMask(), 	3, 3, 1, 1);
		Bl_Pawn_Moves 	= getAttackBitboards(getBlPmMask(), 	3, 3, 1, 1);
		Wh_Pawn_Attacks = getAttackBitboards(getWhPxMask(), 	3, 3, 1, 1);
		Wh_Pawn_Moves 	= getAttackBitboards(getWhPmMask(), 	3, 3, 1, 1);
		Right_Moves	= getAttackBitboards(getRightMask(),   		1, 8, 0, 0);
		Left_Moves	= getAttackBitboards(getLeftMask(),   		1, 8, 0, 7);
		Up_Moves	= getAttackBitboards(getUpMask(),			8, 1, 0, 0);
		Down_Moves	= getAttackBitboards(getDownMask(),			8, 1, 7, 0);
		Up_Left_Moves	= getAttackBitboards(getUpLeftMask(), 	8, 8, 0, 7);
		Down_Left_Moves = getAttackBitboards(getDownLeftMask(), 8, 8, 7, 7);
		Down_Right_Moves= getAttackBitboards(getDownRightMask(),8, 8, 7, 0);
		Up_Right_Moves	= getAttackBitboards(getUpRightMask(), 	8, 8, 0, 0);
		
		BlackKing = 		(long)1 << 60;
		BlackKingRook = 	(long)1 << 63;
		BlackQueenRook = 	(long)1 << 56;
		WhiteKing = 		(long)1 << 4;
		WhiteKingRook = 	(long)1 << 7;
		WhiteQueenRook =	(long)1 << 0;
		
		WhiteQCastle = ((long)1 << 1) | ((long)1 << 2) | ((long)1 << 3);
		WhiteKCastle = ((long)1 << 5) | ((long)1 << 6);
		BlackQCastle = ((long)1 << 57) | ((long)1 << 58) | ((long)1 << 59);
		BlackKCastle = ((long)1 << 61) | ((long)1 << 62);
		//System.out.println("Done generating move maps");
	}
	
	public static long[] getKing_Attacks(){		return King_Attacks;}
	public static long[] getKnight_Attacks(){		return Knight_Attacks;}
	public static long[] getBlPawn_Attacks(){		return Bl_Pawn_Attacks;}
	public static long[] getBlPawn_Moves(){		return Bl_Pawn_Moves;}
	public static long[] getWhPawn_Attacks(){		return Wh_Pawn_Attacks;}
	public static long[] getWhPawn_Moves(){		return Wh_Pawn_Moves;}
	public static long[] getRight_Moves(){			return Right_Moves;}
	public static long[] getLeft_Moves(){			return Left_Moves;}
	public static long[] getUp_Moves(){			return Up_Moves;}
	public static long[] getDown_Moves(){			return Down_Moves;}
	public static long[] getUpLeft_Moves(){		return Up_Left_Moves;}
	public static long[] getDownLeft_Moves(){		return Down_Left_Moves;}
	public static long[] getDownRight_Moves(){		return Down_Right_Moves;}
	public static long[] getUpRight_Moves(){		return Up_Right_Moves;}
	
	@SuppressWarnings("unused")
	private void printMask(byte[][] mask, int r, int c){
		for(int i = 0; i < r; i++){
			for(int q = 0; q < c; q++){
				System.out.print(mask[i][q]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	private static byte[][] getRightMask(){
		byte[][] Right_Mask = makeInitMask(1, 8, (byte)1);
		Right_Mask[0][0] = 0;
		//System.out.println("RightMask");printMask(Right_Mask, 1, 8);
		return Right_Mask;
	}
	private static byte[][] getLeftMask(){
		byte[][] Left_Mask = makeInitMask(1, 8, (byte)1);
		Left_Mask[0][7] = 0;
		//System.out.println("LeftMask");printMask(Left_Mask, 1, 8);
		return Left_Mask;
	}
	private static byte[][] getUpMask(){
		byte[][] Up_Mask = makeInitMask(8, 1, (byte)1);
		Up_Mask[0][0] = 0;
		//System.out.println("UpMask");printMask(Up_Mask, 8, 1);
		return Up_Mask;
	}
	private static byte[][] getDownMask(){
		byte[][] Down_Mask = makeInitMask(8, 1, (byte)1);
		Down_Mask[7][0] = 0;
		//System.out.println("DownMask");printMask(Down_Mask, 8, 1);
		return Down_Mask;
	}
	private static byte[][] getUpLeftMask(){
		byte[][] Up_Left_Mask = makeInitMask(8, 8, (byte)0);
		for(int i = 0; i < 8; i++)
			Up_Left_Mask[i][8-(i+1)] = 1;
		Up_Left_Mask[0][7] = 0;
		//System.out.println("UpLeftMask");printMask(Up_Left_Mask, 8, 8);
		return Up_Left_Mask;
	}
	private static byte[][] getDownLeftMask(){
		byte[][] Down_Left_Mask = makeInitMask(8, 8, (byte)0);
		for(int i = 0; i < 8; i++)
			Down_Left_Mask[i][i] = 1;
		Down_Left_Mask[7][7] = 0;
		//System.out.println("DownLeftMask");printMask(Down_Left_Mask, 8, 8);
		return Down_Left_Mask;
	}
	private static byte[][] getDownRightMask(){
		byte[][] Down_Right_Mask = makeInitMask(8, 8, (byte)0);
		for(int i = 0; i < 8; i++)
			Down_Right_Mask[i][8-(i+1)] = 1;
		Down_Right_Mask[7][0] = 0;
		//System.out.println("DownRightMask");printMask(Down_Right_Mask, 8, 8);
		return Down_Right_Mask;
	}
	private static byte[][] getUpRightMask(){
		byte[][] Up_Right_Mask = makeInitMask(8, 8, (byte)0);
		for(int i = 0; i < 8; i++)
			Up_Right_Mask[i][i] = 1;
		Up_Right_Mask[0][0] = 0;
		//System.out.println("UpRightMask");printMask(Up_Right_Mask, 8, 8);
		return Up_Right_Mask;
	}
	
	private static byte[][] getKingMask(){
		byte[][] King_Mask = makeInitMask(3, 3, (byte)1);
		King_Mask[1][1] = 0;
		//System.out.println("KingMask");printMask(King_Mask, 3, 3);
		return King_Mask;
	}
	private static byte[][] getWhPxMask(){
		byte[][] Pawn_Mask = makeInitMask(3, 3, (byte)0);
		Pawn_Mask[2][0] = 1;
		Pawn_Mask[2][2] = 1;
		//System.out.println("WhPXMask");printMask(Pawn_Mask, 3, 3);
		return Pawn_Mask;
	}
	private static byte[][] getWhPmMask(){
		byte[][] Pawn_Mask = makeInitMask(3, 3, (byte)0);
		Pawn_Mask[2][1] = 1;
		//System.out.println("WhPawnMask");printMask(Pawn_Mask, 3, 3);
		return Pawn_Mask;
	}
	private static byte[][] getBlPxMask(){
		byte[][] Pawn_Mask = makeInitMask(3, 3, (byte)0);
		Pawn_Mask[0][0] = 1;
		Pawn_Mask[0][2] = 1;
		//System.out.println("BlPXMask");printMask(Pawn_Mask, 3, 3);
		return Pawn_Mask;
	}
	private static byte[][] getBlPmMask(){
		byte[][] Pawn_Mask = makeInitMask(3, 3, (byte)0);
		Pawn_Mask[0][1] = 1;
		//System.out.println("BlPawnMask");printMask(Pawn_Mask, 3, 3);
		return Pawn_Mask;
	}
	private static byte[][] getKnightMask(){
		byte[][] Knight_Mask = makeInitMask(5, 5, (byte)0);
		Knight_Mask[1][0] = 1;
		Knight_Mask[0][1] = 1;
		Knight_Mask[3][0] = 1;
		Knight_Mask[0][3] = 1;
		Knight_Mask[1][4] = 1;
		Knight_Mask[4][1] = 1;
		Knight_Mask[3][4] = 1;
		Knight_Mask[4][3] = 1;	
		//System.out.println("KnightMask");printMask(Knight_Mask, 5, 5);
		return Knight_Mask;
	}
	private static byte[][] makeInitMask(int i, int q, byte initTO){
		byte[][] mask = new byte[i][q];
		for (int r = 0; r < i; r++){
			for( int c = 0; c < q; c++){
				mask[r][c] = initTO;
			}
		}
		return mask;
	}

	private static long[] getAttackBitboards(byte[][] MASK, int maskSizeR, int maskSizeC, int offsetR, int offsetC){
		
		long[] Attacks = new long[64];
		byte[][][] Attack_Array_Boards = new byte[64][8][8];
		
		//initialize to zero
		for(int b = 0; b < 64; b++){
			Attack_Array_Boards[b] = makeInitMask(8, 8, (byte)0);
		}

		//each square on the attack_array_board
		for(int r = 0; r < 8; r++){							
			for(int c = 0; c < 8; c++){
				
				//each square of mask
				for(int maskR = 0; maskR < maskSizeR; maskR++){
					for(int maskC = 0; maskC < maskSizeC; maskC++){
						
						//the current superimposed pixel
						int subR = r - offsetR + maskR;
						int subC = c - offsetC + maskC;
						
						try{Attack_Array_Boards[r*8+c][subR][subC] = MASK[maskR][maskC];}
						catch(ArrayIndexOutOfBoundsException e){}
					}
				}
			}
		}
		
		//PRINTING BYTES[][] BOARD
		for(int b = 0; b < 64; b++){
			for(int r = 0; r < 8; r++){
				for(int c = 0; c < 8; c++){
					//System.out.print(Attack_Array_Boards[b][r][c]);
				}
				//System.out.println();
			}
			//System.out.println();
		}
		
		//convert the {1,0} Array_boards into (bitboard) longs
		for(int b = 0; b < 64; b++){
			String s = "";
			for(int r = 0; r < 8; r++){
				for( int c = 7; c >= 0; c--){
					s += Attack_Array_Boards[b][r][c];
				}
			}
			
			//try to set the long if the first digit isn't 1
			try{
				Attacks[b] = Long.parseLong(s, 2);
			}catch(NumberFormatException e){
				
				//invert all ones and zeros, add 1, negate
				//two's complement rules
				for(int i = 0; i < s.length(); i++){
					if(s.charAt(i) == '0')
						s = s.substring(0, i) + "1" + s.substring(i+1, s.length());
					else
						s = s.substring(0, i) + "0" + s.substring(i+1, s.length());
				}
				Attacks[b] = (Long.parseLong(s, 2) + 1) * -1;
			}
			Attacks[b] = Long.reverseBytes(Attacks[b]);
		}
		return Attacks;
	}
		
		
}

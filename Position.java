package game2;
import java.util.ArrayList;

public class Position {

	Bitboards myBitboards;
	GameInfo myGameInfo;
	
	int ValueWhite;
	int ValueBlack;
	
    public ArrayList<Move> CheckTestMoves(boolean WHITE, java.util.ArrayList<Move> moves){
    	if(WHITE){
    		for(int i = 0; i < moves.size(); i++){
    			makeMove(moves.get(i));
    			if(Long.bitCount(checkTestWhite()) != 0){
    				undoMove(moves.get(i));
    				moves.remove(i);
    				i--;
    			}
    			else
    				undoMove(moves.get(i));
    		}
    	}
    	else{
    		for(int i = 0; i < moves.size(); i++){
    			makeMove(moves.get(i));
    			if(Long.bitCount(checkTestBlack()) != 0){
    				undoMove(moves.get(i));
    				moves.remove(i);
    				i--;
    			}
    			else
    				undoMove(moves.get(i));
    		}
    	}
    	return moves;
    }
	
    public long checkTestWhiteSquare(int BIT){
		MoveGen blackAttack = new MoveGen(this);
		blackAttack.GenerateBlack();
		ArrayList<Move> moves = blackAttack.getMoves();
		long ATTACK = 0;
		for(int i = 0; i < moves.size(); i++){
			if (moves.get(i).getTo() == BIT)
				ATTACK = ATTACK | (long)1 << (moves.get(i).getTo());
		}
		return ATTACK;
	}
    
	public long checkTestWhite(){
		
		MoveGen blackAttack = new MoveGen(this);
		blackAttack.GenerateBlack();
		ArrayList<Move> moves = blackAttack.getMoves();
		long ATTACK = 0;
		for(int i = 0; i < moves.size(); i++){
			if (moves.get(i).getTo() == Long.numberOfTrailingZeros(myBitboards.WHITE_KING))
				ATTACK = ATTACK | (long)1 << (moves.get(i).getTo());
		}
		return ATTACK;
	}
	
	public long checkTestBlackSquare(int BIT){
		MoveGen whiteAttack = new MoveGen(this);
		whiteAttack.GenerateWhite();
		ArrayList<Move> moves = whiteAttack.getMoves();
		long ATTACK = 0;
		for(int i = 0; i < moves.size(); i++){
			if (moves.get(i).getTo() == BIT)
				ATTACK = ATTACK | (long)1 << (moves.get(i).getTo());
		}
		return ATTACK;
	}
	
	public long checkTestBlack(){
		MoveGen whiteAttack = new MoveGen(this);
		whiteAttack.GenerateWhite();
		ArrayList<Move> moves = whiteAttack.getMoves();
		long ATTACK = 0;
		for(int i = 0; i < moves.size(); i++){
			if (moves.get(i).getTo() == Long.numberOfTrailingZeros(myBitboards.BLACK_KING))
				ATTACK = ATTACK | (long)1 << (moves.get(i).getTo());
		}
		return ATTACK;
	}
	
	Position(){
		System.out.println("In Position()");
		myBitboards = new Bitboards();
		myGameInfo = new GameInfo();
		
		ValueWhite = 0;
		ValueBlack = 0;
		System.out.println("Out of Position()");
	}
	Position(Position p){
		myBitboards = new Bitboards(p.myBitboards);
		myGameInfo = new GameInfo(p.myGameInfo);
		
		ValueWhite = p.ValueWhite;
		ValueBlack = p.ValueBlack;
	}
	Position(Bitboards B){
		myBitboards = B;
		myGameInfo = new GameInfo();
	}
	Position(Pieceboard P){
		myBitboards = new Bitboards(P);
		myGameInfo = new GameInfo();
	}

	//  100	310	320	500 1000 10000
	//	p	n	b	r	q	 k
	
	public void evaluate(){
				
		//		MATERIAL VALUE
		ValueBlack = 100 *	(Long.bitCount(myBitboards.BLACK_PAWNS) - Long.bitCount(myBitboards.WHITE_PAWNS));
		ValueBlack +=310 *	(Long.bitCount(myBitboards.BLACK_KNIGHTS) - Long.bitCount(myBitboards.WHITE_KNIGHTS));
		ValueBlack +=320 *	(Long.bitCount(myBitboards.BLACK_BISHOPS) - Long.bitCount(myBitboards.WHITE_BISHOPS));
		ValueBlack +=500 *	(Long.bitCount(myBitboards.BLACK_ROOKS) - Long.bitCount(myBitboards.WHITE_ROOKS));
		ValueBlack +=1000*	(Long.bitCount(myBitboards.BLACK_QUEENS) - Long.bitCount(myBitboards.WHITE_QUEENS));
		ValueBlack +=10000*	(Long.bitCount(myBitboards.BLACK_KING) - Long.bitCount(myBitboards.WHITE_KING));

		ValueWhite = 0 - ValueBlack;
		
		//Pawns in front of Kings
		ValueBlack += 10*(myBitboards.BLACK_KING >>> 7) & myBitboards.BLACK_PAWNS;
			ValueWhite -= 10*(myBitboards.BLACK_KING >>> 7) & myBitboards.BLACK_PAWNS;
		ValueBlack += 10*(myBitboards.BLACK_KING >>> 8) & myBitboards.BLACK_PAWNS;
			ValueWhite -= 10*(myBitboards.BLACK_KING >>> 8) & myBitboards.BLACK_PAWNS;
		ValueBlack += 10*(myBitboards.BLACK_KING >>> 9) & myBitboards.BLACK_PAWNS;
			ValueWhite -= 10*(myBitboards.BLACK_KING >>> 9) & myBitboards.BLACK_PAWNS;
		
		ValueWhite += 10*(myBitboards.WHITE_KING >>> 7) & myBitboards.WHITE_PAWNS;
			ValueBlack -= 10*(myBitboards.WHITE_KING >>> 7) & myBitboards.WHITE_PAWNS;
		ValueWhite += 10*(myBitboards.WHITE_KING >>> 8) & myBitboards.WHITE_PAWNS;
			ValueBlack -= 10*(myBitboards.WHITE_KING >>> 8) & myBitboards.WHITE_PAWNS;
		ValueWhite += 10*(myBitboards.WHITE_KING >>> 9) & myBitboards.WHITE_PAWNS;
			ValueBlack -= 10*(myBitboards.WHITE_KING >>> 9) & myBitboards.WHITE_PAWNS;
		
		//Threat board
		Position whitePos = new Position(this);
		whitePos.myBitboards.BLACK_PIECES = whitePos.myBitboards.ALL_PIECES;
		
		Position blackPos = new Position(this);
		blackPos.myBitboards.WHITE_PIECES = blackPos.myBitboards.ALL_PIECES;
		
		MoveGen whiteGen = new MoveGen(whitePos);
		whiteGen.GenerateWhite();
		ArrayList<Move> whiteMoves = whiteGen.getMoves();
		
		MoveGen blackGen = new MoveGen(blackPos);
		blackGen.GenerateBlack();
		ArrayList<Move> blackMoves = blackGen.getMoves();
		
		//for every square that white attacks (including defense-squares)
		for(int i = 0; i < whiteMoves.size(); i++){
			Move myMove = whiteMoves.get(i);
			
			if (myMove.getTakes() == 1){
				long toBoard = (long)1 << myMove.getTo();
				ValueWhite += 10 * (Long.bitCount(toBoard & (myBitboards.BLACK_PAWNS | myBitboards.WHITE_PAWNS)));
				ValueWhite += 31 * (Long.bitCount(toBoard & (myBitboards.BLACK_KNIGHTS | myBitboards.WHITE_KNIGHTS)));
				ValueWhite += 32 * (Long.bitCount(toBoard & (myBitboards.BLACK_BISHOPS | myBitboards.WHITE_BISHOPS)));
				ValueWhite += 50 * (Long.bitCount(toBoard & (myBitboards.BLACK_ROOKS | myBitboards.WHITE_ROOKS)));
				ValueWhite += 100* (Long.bitCount(toBoard & (myBitboards.BLACK_QUEENS | myBitboards.WHITE_QUEENS)));
				ValueWhite += 300*(Long.bitCount(toBoard & myBitboards.BLACK_KING));
				ValueBlack -= 10 * (Long.bitCount(toBoard & (myBitboards.BLACK_PAWNS | myBitboards.WHITE_PAWNS)));
				ValueBlack -= 31 * (Long.bitCount(toBoard & (myBitboards.BLACK_KNIGHTS | myBitboards.WHITE_KNIGHTS)));
				ValueBlack -= 32 * (Long.bitCount(toBoard & (myBitboards.BLACK_BISHOPS | myBitboards.WHITE_BISHOPS)));
				ValueBlack -= 50 * (Long.bitCount(toBoard & (myBitboards.BLACK_ROOKS | myBitboards.WHITE_ROOKS)));
				ValueBlack -= 100* (Long.bitCount(toBoard & (myBitboards.BLACK_QUEENS | myBitboards.WHITE_QUEENS)));
				ValueBlack -= 300*(Long.bitCount(toBoard & myBitboards.BLACK_KING));
			}else{
				ValueWhite += 5;
				ValueBlack -= 5;
			}
		}
		
		for(int i = 0; i < blackMoves.size(); i++){
			
			Move myMove = blackMoves.get(i);
			
			if (myMove.getTakes() == 1){
				long toBoard = (long)1 << myMove.getTo();
				ValueBlack += 10 * (Long.bitCount(toBoard & (myBitboards.BLACK_PAWNS | myBitboards.WHITE_PAWNS)));
				ValueBlack += 31 * (Long.bitCount(toBoard & (myBitboards.BLACK_KNIGHTS | myBitboards.WHITE_KNIGHTS)));
				ValueBlack += 32 * (Long.bitCount(toBoard & (myBitboards.BLACK_BISHOPS | myBitboards.WHITE_BISHOPS)));
				ValueBlack += 50 * (Long.bitCount(toBoard & (myBitboards.BLACK_ROOKS | myBitboards.WHITE_ROOKS)));
				ValueBlack += 100* (Long.bitCount(toBoard & (myBitboards.BLACK_QUEENS | myBitboards.WHITE_QUEENS)));
				ValueBlack += 300* (Long.bitCount(toBoard & myBitboards.WHITE_KING));
				ValueWhite -= 10 * (Long.bitCount(toBoard & (myBitboards.BLACK_PAWNS | myBitboards.WHITE_PAWNS)));
				ValueWhite -= 31 * (Long.bitCount(toBoard & (myBitboards.BLACK_KNIGHTS | myBitboards.WHITE_KNIGHTS)));
				ValueWhite -= 32 * (Long.bitCount(toBoard & (myBitboards.BLACK_BISHOPS | myBitboards.WHITE_BISHOPS)));
				ValueWhite -= 50 * (Long.bitCount(toBoard & (myBitboards.BLACK_ROOKS | myBitboards.WHITE_ROOKS)));
				ValueWhite -= 100* (Long.bitCount(toBoard & (myBitboards.BLACK_QUEENS | myBitboards.WHITE_QUEENS)));
				ValueWhite -= 300* (Long.bitCount(toBoard & myBitboards.WHITE_KING));
			}else{
				ValueBlack += 5;
				ValueWhite -= 5;
			}
		}
		if(ValueWhite > 10000){
			ValueWhite = 10000;
			ValueBlack = -10000;
		}
		if(ValueBlack > 10000){
			ValueWhite = -10000;
			ValueBlack = 10000;
		}
		System.out.println("White = " + ValueWhite);
		System.out.println("Black = " + ValueBlack);
	}
	
	public void printBoard(){
		myBitboards.printBoard();
	}
	
	public void makeMove(Move M){
		M.setGameState(myGameInfo.getMyData());
		
		myBitboards.makeMove(M);
		myGameInfo.makeMove(M);
	}
	
	public void undoMove(Move M){
		
		myBitboards.undoMove(M);
		myGameInfo.undoMove(M);
	}

}

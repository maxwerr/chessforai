package game2;

public class MoveTranslate {

	Move moveIn;
	String command;
	
	int moveFromRow;
	char moveFromCol;
	int moveToRow;
	char moveToCol;
	
	public int toSquare(char a, char b){
		int row = b - 1;
		int col = a - 'a';
		return (row*8 + col);
	}
	
	public void displayMove(){
		System.out.println("human = " + command);
		//System.out.println("cpuMov= " + moveIn.getEncoded());
	}
	
	MoveTranslate(Move M){
		
		moveIn = M;
		int moveFromRow 	= 	(int) (1 + 		(M.getFrom() << 3));
		char moveFromCol 	= 	(char)('a' + 	(M.getFrom() & 7));
		int moveToRow 		= 	(int) (1 +		(M.getTo() << 3));
		char moveToCol 		= 	(char)('a' + 	(M.getTo() & 7));
		
		if(M.getRank() == 6)
			command = "0-0";
		else if(M.getRank() == 7)
			command = "0-0-0";
		else{
			if(M.getTakes() == 1)
				command = moveFromCol + "" + moveFromRow + "x" + moveToCol + "" + moveToRow;
			else
				command = moveFromCol + "" + moveFromRow + "" + moveToCol + "" + moveToRow;
			
			switch((int)M.getRank()){
			case Move.pawn:; break;
			case Move.king: command = "K" + command; break;
			case Move.knight: command = "N" + command; break;
			case Move.bishop: command = "B" + command; break;
			case Move.queen: command = "Q" + command; break;
			case Move.rook: command = "R" + command; break;
			default:; break;
			}
		}
	}
	
	public int parseNum(String S){
		try{
			return S.charAt(S.length()-1);
		}
		catch(IndexOutOfBoundsException e){
			return -1;
		}
	}
	public int parseLet(String S){
		try{
			if(S.charAt(S.length()-1) > 0 && S.charAt(S.length()-1) < 9)
				return S.charAt(S.length()-1);
			else
				return -1;
		}
		catch(IndexOutOfBoundsException e){
			return -1;
		}
	}
	public char parseX(String S){
		try{
			if(S.charAt(S.length()-1) == 'x')
				return S.charAt(S.length()-1);
			else
				return (char) -1;
		}
		catch(IndexOutOfBoundsException e){
			return (char) -1;
		}
	}
	public char parseRank(String S){
		try{
			if(S.charAt(S.length()-1) == 'K' || S.charAt(S.length()-1) == 'Q' || S.charAt(S.length()-1) == 'N' || S.charAt(S.length()-1) == 'B' || S.charAt(S.length()-1) == 'R')
				return S.charAt(S.length()-1);
			else
				return (char) -1;
		}
		catch(IndexOutOfBoundsException e){
			return (char) -1;
		}
	}
	
	MoveTranslate(String T){
		
		if(T.length() < 2 || T.length() > 6){
			System.out.println("Your move is invalid");
			//Call whoever prompted user for string
		}
		/*
		int PieceKind;
		int toSquare;
		int fromSquare;
		
		if( T == "0-0-0")
			PieceKind = Move.qsCastle;
		else if( T == "0-0")
			PieceKind = Move.ksCastle;
		else
			toSquare = toSquare(T.charAt(T.length()-2), T.charAt(T.length()-1));
		*/
		
		/*
		else if(T.length() == 2){
			Piece = 'P';
			moveToRow = T.charAt(T.length()-1) - 1;
			moveToCol = T.charAt(T.length()-2) - 'a';
		}
		else if(T.length() == 4){
			moveToRow 	= T.charAt(T.length()-1) - 1;
			moveToCol 	= T.charAt(T.length()-2) - 'a';
			moveFromRow = T.charAt(T.length()-3) - 1;
			moveFromCol = T.charAt(T.length()-4) - 'a';
		}
		else if(T.length() == 5 && T.length() - 3 == 'x'){
			moveToRow 	= T.charAt(T.length()-1) - 1;
			moveToCol 	= T.charAt(T.length()-2) - 'a';
			Takes = true;
			moveFromRow = T.charAt(T.length()-4) - 1;
			moveFromCol = T.charAt(T.length()-5) - 'a';
		}
		else if(T.length() == 5 && T.length() - 3 != 'x'){
			moveToRow 	= T.charAt(T.length()-1) - 1;
			moveToCol 	= T.charAt(T.length()-2) - 'a';
			moveFromRow = T.charAt(T.length()-3) - 1;
			moveFromCol = T.charAt(T.length()-4) - 'a';
			Piece 	= T.charAt(T.length() - 5);
		}
		else if(T.length() == 6 && T.length() - 3 == 'x'){
			moveToRow 	= T.charAt(T.length()-1) - 1;
			moveToCol 	= T.charAt(T.length()-2) - 'a';
			Takes = true;
			moveFromRow = T.charAt(T.length()-4) - 1;
			moveFromCol = T.charAt(T.length()-5) - 'a';
			Piece 	= T.charAt(T.length() - 6);
		}
		*/
	}
}

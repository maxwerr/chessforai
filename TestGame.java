package game2;

import java.util.ArrayList;

public class TestGame {
	
	MoveGen myGen;
	Position myPos;
	ArrayList<Move> myMoves;
	ArrayList<Move> myHistory;
	public static final int idealDepth = 2;
	AB myAB;
	
	TestGame(){
		System.out.println("in TestGame()");
		myPos = new Position();
		myGen = new MoveGen(myPos);
		myHistory = new ArrayList<Move>();
		System.out.println("out of TestGame()");
		myAB = new AB(idealDepth, myPos, false);
	}
	public static void main(String args[]){
		System.out.println("in Main()");
		TestGame myGame = new TestGame();
		
		myGame.MoveGenTesting();
		
	}
	
	public void MoveGenTesting(){
		
		MoveMaps.makeMaps();
		
		myPos.printBoard();
		
		turn();
		
	}
	
	
	
	public void turn(){
		int movesEach = 5;
		for(int i = 0; i < movesEach; i++){
			myPos.evaluate();
			whiteTurn();
			myPos.evaluate();
			blackTurn();
			//alphaBetaTurn();
			System.out.println("\nHistory");
			printMoves(myHistory);
		}
		for(int i = 0; i < movesEach*2; i++){
			printMoves(myHistory);
			java.util.Scanner kb = new java.util.Scanner(System.in);
			int undo = kb.nextInt();
			myPos.undoMove(myHistory.get(undo));
			myPos.printBoard();
			myHistory.remove(undo);
		}
	}
	public void whiteTurn(){
		myGen.GenerateWhite();
		myMoves = myGen.getMoves();
		myMoves = myPos.CheckTestMoves(true, myMoves);
		printMoves(myMoves);
		java.util.Scanner kb = new java.util.Scanner(System.in);
		System.out.println("move number: ");
		int i = kb.nextInt();
		TranslateMove tranny = new TranslateMove(myMoves.get(i));
		System.out.println("making move " + tranny.myString + "...");
		myPos.makeMove(myMoves.get(i));
		myHistory.add(myMoves.get(i));
		myPos.printBoard();
		myGen.clearMoves();
	}
	public void alphaBetaTurn(){
		System.out.print("My move is...");
		myAB.runner();
		Move m = myAB.getBestMove();
		TranslateMove tranny = new TranslateMove(m);
		System.out.println(tranny.myString);
		myPos.printBoard();
	}
	public void blackTurn(){
		myGen.GenerateBlack();
		myMoves = myGen.getMoves();
		myMoves = myPos.CheckTestMoves(false, myMoves);
		printMoves(myMoves);
		java.util.Scanner kb = new java.util.Scanner(System.in);
		int i = kb.nextInt();
		TranslateMove tranny = new TranslateMove(myMoves.get(i));
		System.out.println("making move " + tranny.myString + "...");
		myPos.makeMove(myMoves.get(i));
		myHistory.add(myMoves.get(i));
		myPos.printBoard();
		myGen.clearMoves();
	}
	public void printMoves(ArrayList<Move> myMoves){
		System.out.println(" MOVE\t\t\t\tINFO\t\tGAME STATE");
		for(int i = 0; i < myMoves.size(); i++){
			TranslateMove trans = new TranslateMove(myMoves.get(i));
			
			System.out.print(i + "---" + trans.myString + "---" + "\t\t");
			myMoves.get(i).printMove();
			System.out.println();
		}
	}
	
	@SuppressWarnings("unused")
	public void MoveMapsTesting(){
		
		Bitboards myBoards = new Bitboards();
		myBoards.printBoard();
		
		long[] king 		= MoveMaps.getKing_Attacks();
		long[] knight 		= MoveMaps.getKnight_Attacks();
		long[] blPawnX 		= MoveMaps.getBlPawn_Attacks();
		long[] blPawnM 		= MoveMaps.getBlPawn_Moves();
		long[] whPawnX 		= MoveMaps.getWhPawn_Attacks();
		long[] whPawnM 		= MoveMaps.getWhPawn_Moves();
		long[] RightMv 		= MoveMaps.getRight_Moves();
		long[] LeftMv 		= MoveMaps.getLeft_Moves();
		long[] UpMv 		= MoveMaps.getUp_Moves();
		long[] DownMv 		= MoveMaps.getDown_Moves();
		long[] UpLeftMv 	= MoveMaps.getUpLeft_Moves();
		long[] DownLeftMv 	= MoveMaps.getDownLeft_Moves();
		long[] UpRightMv 	= MoveMaps.getUpRight_Moves();
		long[] DownRightMv 	= MoveMaps.getDownRight_Moves();
		
		System.out.println();
		
		printBitBoardForward(MoveMaps.WhiteKing);
		
		
		for(int i = 8; i < 17; i++){
			//System.out.println("i = " + i);
			//printBitBoardForward(king[i]);
		}
	}

	public static void printBitBoardForward(long myLong){
		int zeros = Long.numberOfLeadingZeros(myLong);
		String bboard = "";
		while(zeros > 0){
			bboard += "0";
			zeros--;
		}
		bboard += Long.toBinaryString(myLong);
		for(int i = 0; i < 8; i++){
			for(int q = 7; q >= 0; q--){
				System.out.print(" " + bboard.charAt(i*8+q ) + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
}
